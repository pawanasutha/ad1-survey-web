import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:survey_app/state_management/idm_notifier.dart';

import 'package:survey_app/utils/custom_widgets/custom_widgets.dart';
import 'package:survey_app/utils/utils.dart';

part 'need_help_section.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: [header(context), loginArea(context)],
      ),
    );
  }

  Widget header(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      width: _sizeScreen.wp(50),
      child: Stack(
        children: [
          ///Background
          Container(
            decoration: BoxDecoration(
                color: Color(0xFE39225A),
                image: DecorationImage(
                    image: AssetImage('assets/images/image_half_rounded.png'),
                    fit: BoxFit.fitHeight)),
          ),

          ///HeaderIconAndText
          Padding(
            padding: EdgeInsets.only(
                top: _sizeScreen.hp(5),
                left: _sizeScreen.wp(5),
                right: _sizeScreen.wp(10)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image(
                  image: AssetImage('assets/icons/icon_adira.png'),
                ),
                SizedBox(
                  height: _sizeScreen.hp(3),
                ),
                Text(
                  'Selamat datang di aplikasi AdiraOneSurvey',
                  style: GoogleFonts.poppins().copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                      fontSize: 24),
                ),
              ],
            ),
          ),

          ///Icon Bottom
          Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
              padding: EdgeInsets.only(
                  bottom: _sizeScreen.hp(5), left: _sizeScreen.wp(5)),
              child: Image(
                image: AssetImage('assets/icons/icon_door.png'),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget loginArea(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      width: _sizeScreen.wp(50),
      child: Consumer<IDMNotifier>(
        builder: (context, idm, _) {
          return WillPopScope(
            onWillPop: () async {
              return false;
            },
            child: Scaffold(
              backgroundColor: Colors.white,
              body: Center(
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: _sizeScreen.wp(8)),
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Text(
                          'Selamat datang di aplikasi AdiraOne Survey. Mohon masukkan akun Anda.',
                          style: poppinsw600sz18,
                        ),
                        SizedBox(height: _sizeScreen.hp(5)),
                        Form(
                          key: Provider.of<IDMNotifier>(context, listen: false)
                              .globalLoginKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              ///NIK
                              Container(
                                margin: EdgeInsets.symmetric(vertical: 1),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "NIK",
                                      style: nunitoSansw700sz12,
                                    ),
                                    SizedBox(
                                      height: _sizeScreen.hp(1),
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8.0)),
                                      child: TextFormField(
                                        enabled: idm.widgetEnable,
                                        controller: idm.controllerEmail,
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        autovalidateMode:
                                            AutovalidateMode.onUserInteraction,
                                        validator: (value) =>
                                            Provider.of<Validation>(context,
                                                    listen: false)
                                                .stringValidation(value!),
                                        style: GoogleFonts.nunitoSans(
                                          fontSize: 14,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black,
                                        ),
                                        decoration: InputDecoration(
                                          fillColor: Color(0xFEF6F9FE),
                                          filled: true,
                                          hintText: 'Ketik NIK',
                                          hintStyle: nunitoSansw400sz14,
                                          enabledBorder: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          errorBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8.0),
                                              borderSide: BorderSide(
                                                  color: Colors.red)),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: _sizeScreen.hp(3)),

                              ///Password
                              Container(
                                margin: EdgeInsets.symmetric(vertical: 1),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      'Kata Sandi',
                                      style: nunitoSansw700sz12,
                                    ),
                                    SizedBox(
                                      height: _sizeScreen.hp(1),
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(8.0)),
                                      child: TextFormField(
                                        enabled: idm.widgetEnable,
                                        enableSuggestions: false,
                                        controller: idm.controllerPassword,
                                        style: GoogleFonts.nunitoSans(
                                          fontSize: 14,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black,
                                        ),
                                        keyboardType:
                                            TextInputType.visiblePassword,
                                        obscureText: idm.secureText,
                                        autovalidateMode:
                                            AutovalidateMode.onUserInteraction,
                                        validator: (value) =>
                                            Provider.of<Validation>(context,
                                                    listen: false)
                                                .stringValidation(value!),
                                        decoration: InputDecoration(
                                          fillColor: Color(0xFEF6F9FE),
                                          filled: true,
                                          hintText: 'Tulis kata sandi',
                                          hintStyle: nunitoSansw400sz14,
                                          enabledBorder: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          errorBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(8.0),
                                              borderSide: BorderSide(
                                                  color: Colors.red)),
                                          suffixIcon: IconButton(
                                              padding: EdgeInsets.only(
                                                  right: _sizeScreen.wp(1.5)),
                                              icon: Icon(
                                                idm.secureText
                                                    ? Icons.visibility_off
                                                    : Icons.visibility,
                                                color: Color(0xFE0A66C2),
                                              ),
                                              onPressed: idm.showHidePass),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: _sizeScreen.hp(2)),

                              ///Remember Me & Forgot Password
                              idm.loginProcess
                                  ? Center(child: Container())
                                  : Container(
                                      width: double.infinity,
                                      child: Row(
                                        children: [
                                          Container(
                                            width: _sizeScreen.wp(20),
                                            child: Theme(
                                              data: ThemeData(
                                                unselectedWidgetColor: Colors
                                                    .black, // Border Color
                                              ),
                                              child: CheckboxListTile(
                                                  contentPadding:
                                                      EdgeInsets.zero,
                                                  controlAffinity:
                                                      ListTileControlAffinity
                                                          .leading,
                                                  title: Text("Ingat Saya",
                                                      style:
                                                          nunitoSansw600sz12),
                                                  value: idm.rememberMe,
                                                  onChanged: (newValue) {
                                                    setState(() {
                                                      idm.rememberMe =
                                                          newValue!;
                                                      debugPrint(
                                                          newValue.toString());
                                                    });
                                                  }),
                                            ),
                                          ),
                                          Spacer(),
                                          idm.loginProcess
                                              ? Center(child: Container())
                                              : Container(
                                                  width: _sizeScreen.wp(12),
                                                  child: InkWell(
                                                    onTap: () {
                                                      Navigator.pushNamed(
                                                          context,
                                                          AppLevelConstants
                                                              .forgotPasswordRoute);
                                                    },
                                                    child: Text('Lupa Sandi?',
                                                        textAlign:
                                                            TextAlign.right,
                                                        style:
                                                            nunitoSansw600sz14C0A66C2),
                                                  ),
                                                ),
                                        ],
                                      ),
                                    ),
                              SizedBox(height: _sizeScreen.hp(2)),

                              ///Button Login
                              idm.loginProcess
                                  ? Center(child: CircularProgressIndicator())
                                  : ElevatedButton(
                                      onPressed: () =>
                                          idm.authenticationUser(context),
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all<
                                                RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(40))),
                                        padding: MaterialStateProperty.all<
                                            EdgeInsets>(EdgeInsets.all(0.0)),
                                        backgroundColor:
                                            MaterialStateProperty.all(
                                                Color(0xFEFFDD00)),
                                      ),
                                      child: Container(
                                          constraints: const BoxConstraints(
                                              minWidth: 100.0, minHeight: 50.0),
                                          // min sizes for Material buttons
                                          alignment: Alignment.center,
                                          child: Text('Masuk',
                                              style: nunitoSansw700sz16)),
                                    ),
                              SizedBox(height: _sizeScreen.hp(4)),

                              ///Bantuan
                            ],
                          ),
                        ),
                        idm.loginProcess ? Container() : NeedHelpSection(),
                        /*InkWell(
                          onTap: () {
                            debugPrint(
                                'Width ${_sizeScreen.wp(18)}, Height ${_sizeScreen.hp(30)} && ' +
                                    'Width ${MediaQuery.of(context).size.width.toString()} Height ${MediaQuery.of(context).size.height.toString()}');
                          },
                          child: Text('Calculate Size'),
                        )*/
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
