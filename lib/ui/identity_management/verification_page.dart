import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:get/get.dart';
import 'package:survey_app/state_management/idm_notifier.dart';

import 'package:survey_app/utils/custom_widgets/custom_widgets.dart';
import 'package:survey_app/utils/utils.dart';

class VerificationPage extends StatefulWidget {
  const VerificationPage({Key? key}) : super(key: key);

  @override
  _VerificationPageState createState() => _VerificationPageState();
}

class _VerificationPageState extends State<VerificationPage> {
  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Scaffold(
      body: Row(
        children: [
          header(context),
          Container(width: _sizeScreen.wp(50), child: verification(context))
        ],
      ),
    );
  }

  Widget header(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      width: _sizeScreen.wp(50),
      child: Stack(
        children: [
          ///Background
          Container(
            decoration: BoxDecoration(
                color: Color(0xFE39225A),
                image: DecorationImage(
                    image: AssetImage('assets/images/image_half_rounded.png'),
                    fit: BoxFit.fitHeight)),
          ),

          ///HeaderIconAndText
          Padding(
            padding: EdgeInsets.only(
                top: _sizeScreen.hp(5),
                left: _sizeScreen.wp(5),
                right: _sizeScreen.wp(10)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image(
                  image: AssetImage('assets/icons/icon_adira.png'),
                ),
                SizedBox(
                  height: _sizeScreen.hp(3),
                ),
                Text(
                  'Selamat datang di aplikasi AdiraOneSurvey',
                  style: GoogleFonts.poppins().copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                      fontSize: 24),
                ),
              ],
            ),
          ),

          ///Icon Bottom
          Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
              padding: EdgeInsets.only(
                  bottom: _sizeScreen.hp(5), left: _sizeScreen.wp(5)),
              child: Image(
                image: AssetImage('assets/icons/icon_phone_verification.png'),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget verification(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      child: Column(
        children: [
          Container(
            child: ListTile(
              title: Text(
                'Kembali',
                style: poppinsw600sz20,
              ),
              onTap: () => Future.delayed(Duration.zero, () {
                Navigator.pop(context);
              }),
              leading: Icon(Icons.arrow_back, color: Colors.black),
            ),
          ),
          SizedBox(
            height: _sizeScreen.hp(25),
          ),
          Consumer<IDMNotifier>(
            builder: (context, idm, _) {
              return Container(
                padding: EdgeInsets.only(
                  left: _sizeScreen.wp(10),
                  right: _sizeScreen.wp(10),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Verifikasi',
                      style: poppinsw600sz20C12162C,
                    ),
                    SizedBox(
                      height: _sizeScreen.hp(1),
                    ),
                    Divider(
                      thickness: 1,
                      color: Color(0xffC7C8CD),
                    ),
                    SizedBox(
                      height: _sizeScreen.hp(2),
                    ),
                    Container(
                      child: Text(
                        'Silahkan pilih cara verifikasi akun kamu',
                        style: nunitoSansw400sz18,
                      ),
                    ),
                    SizedBox(
                      height: _sizeScreen.hp(5),
                    ),
                    Container(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomCardVerification(
                              imageAssetPath: 'assets/icons/icon_whatsapp.png',
                              textVerification: 'Whatsapp ke ',
                              phoneNumber: '•••••••••1234',
                              function: () => Get.toNamed(
                                  AppLevelConstants.verificationOTPRoute),
                            ),
                            SizedBox(
                              height: _sizeScreen.hp(2),
                            ),
                            CustomCardVerification(
                              imageAssetPath: 'assets/icons/icon_sms.png',
                              textVerification: 'SMS ke ',
                              phoneNumber: '•••••••••1234',
                              function: () => Get.toNamed(
                                  AppLevelConstants.verificationOTPRoute),
                            ),
                          ]),
                    )
                  ],
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
