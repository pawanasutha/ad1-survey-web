import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:survey_app/state_management/idm_notifier.dart';

import 'package:survey_app/utils/custom_widgets/custom_widgets.dart';
import 'package:survey_app/utils/utils.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({Key? key}) : super(key: key);

  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Scaffold(
      body: Row(
        children: [
          header(context),
          Container(width: _sizeScreen.wp(50), child: forgotPassword(context))
        ],
      ),
    );
  }

  Widget header(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      width: _sizeScreen.wp(50),
      child: Stack(
        children: [
          ///Background
          Container(
            decoration: BoxDecoration(
                color: Color(0xFE39225A),
                image: DecorationImage(
                    image: AssetImage('assets/images/image_half_rounded.png'),
                    fit: BoxFit.fitHeight)),
          ),

          ///HeaderIconAndText
          Padding(
            padding: EdgeInsets.only(
                top: _sizeScreen.hp(5),
                left: _sizeScreen.wp(5),
                right: _sizeScreen.wp(10)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image(
                  image: AssetImage('assets/icons/icon_adira.png'),
                ),
                SizedBox(
                  height: _sizeScreen.hp(3),
                ),
                Text(
                  'Selamat datang di aplikasi AdiraOneSurvey',
                  style: GoogleFonts.poppins().copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                      fontSize: 24),
                ),
              ],
            ),
          ),

          ///Icon Bottom
          Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
              padding: EdgeInsets.only(
                  bottom: _sizeScreen.hp(5), left: _sizeScreen.wp(5)),
              child: Image(
                image: AssetImage('assets/icons/icon_padlock.png'),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget forgotPassword(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      child: Column(
        children: [
          Container(
            child: ListTile(
              title: Text(
                'Kembali',
                style: poppinsw600sz20,
              ),
              onTap: () => Future.delayed(Duration.zero, () {
                Navigator.pop(context);
              }),
              leading: Icon(Icons.arrow_back, color: Colors.black),
            ),
          ),
          SizedBox(
            height: _sizeScreen.hp(30),
          ),
          Consumer<IDMNotifier>(
            builder: (context, idm, _) {
              return Form(
                key: idm.globalForgotPasswordKey,
                child: Container(
                  padding: EdgeInsets.only(
                    left: _sizeScreen.wp(8),
                    right: _sizeScreen.wp(8),
                  ),
                  child: Column(
                    children: [
                      Text(
                        'Instruksi akan dikirimkan via email. Mohon untuk memasukan email yang terdaftar.',
                        style: poppinsw600sz18C4F5262,
                      ),
                      SizedBox(
                        height: _sizeScreen.hp(4),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 1),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'Email Terdaftar',
                              style: nunitoSansw700sz12,
                            ),
                            SizedBox(
                              height: _sizeScreen.hp(2),
                            ),
                            TextFormField(
                              controller: idm.controllerEmailRegistered,
                              style: nunitoSansw400sz14,
                              decoration: new InputDecoration(
                                hintText: 'Tulis email',
                                suffixStyle: nunitoSansw400sz14C92949D,
                                fillColor: Color(0xFEF6F9FE),
                                filled: true,
                                hintStyle: GoogleFonts.nunitoSans(
                                    fontSize: 16,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w500),
                                enabledBorder: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                    borderSide: BorderSide(color: Colors.red)),
                              ),
                              keyboardType: TextInputType.emailAddress,
                              autovalidateMode:
                                  AutovalidateMode.onUserInteraction,
                              validator: (value) => Provider.of<Validation>(
                                      context,
                                      listen: false)
                                  .stringValidation(value ?? ''),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: _sizeScreen.hp(5),
                      ),
                      ElevatedButton(
                        onPressed: () => idm.forgetUser(context),
                        style: ButtonStyle(
                          shape:
                              MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(40))),
                          padding: MaterialStateProperty.all<EdgeInsets>(
                              EdgeInsets.all(0.0)),
                          backgroundColor:
                              MaterialStateProperty.all(Color(0xFEFFDD00)),
                        ),
                        child: Container(
                            constraints: const BoxConstraints(
                                minWidth: 100.0, minHeight: 50.0),
                            // min sizes for Material buttons
                            alignment: Alignment.center,
                            child: Text('Kirim Intruksi',
                                style: nunitoSansw700sz16)),
                      ),
                    ],
                  ),
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
