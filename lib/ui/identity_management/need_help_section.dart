part of 'login_page.dart';

class NeedHelpSection extends StatelessWidget {
  const NeedHelpSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: RichText(
      text: TextSpan(
          style: DefaultTextStyle.of(context).style,
          children: <TextSpan>[
            TextSpan(text: 'Butuh Bantuan? ', style: nunitoSansw400sz16),
            TextSpan(
                text: 'Klik disini',
                style: nunitoSansw600sz14C0A66C2,
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    Provider.of<IDMNotifier>(context, listen: false)
                        .showPopupHelp(context);
                  })
          ]),
    ));
  }
}
