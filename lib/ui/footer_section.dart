import 'package:flutter/material.dart';
import 'package:survey_app/utils/custom_widgets/custom_widgets.dart';
import 'package:survey_app/utils/utils.dart';

class FooterSection extends StatelessWidget {
  const FooterSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      height: _sizeScreen.hp(7),
      width: double.infinity,
      child: Column(
        children: [
          Divider(),
          Padding(
            padding: EdgeInsets.only(
                top: _sizeScreen.hp(0.8), left: _sizeScreen.wp(2)),
            child: Row(
              children: [
                Text(
                  'Ad1Survey © 2021',
                  style: nunitoSansw400sz14C4F5262,
                  overflow: TextOverflow.ellipsis,
                ),
                Spacer(),
                TextButton(
                    onPressed: () {},
                    child: Text(
                      'Pusat Bantuan',
                      style: nunitoSansw400sz14C4F5262,
                      overflow: TextOverflow.ellipsis,
                    )),
                SizedBox(
                  width: _sizeScreen.wp(5),
                ),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    'Syarat & Ketentuan',
                    style: nunitoSansw400sz14C4F5262,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                SizedBox(
                  width: _sizeScreen.wp(5),
                ),
                TextButton(
                  onPressed: () {},
                  child: Text(
                    'Kebijakan Privasi',
                    style: nunitoSansw400sz14C4F5262,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                SizedBox(
                  width: _sizeScreen.wp(2),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
