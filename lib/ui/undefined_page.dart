import 'package:flutter/material.dart';

class UndefinedView extends StatelessWidget {
  const UndefinedView({Key? key, this.name}) : super(key: key);

  /// Name of the route....
  final String? name;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('Something went wrong for $name'),
      ),
    );
  }
}
