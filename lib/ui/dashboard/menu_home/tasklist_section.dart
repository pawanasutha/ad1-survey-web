part of 'home_page.dart';

class TaskListSection extends StatelessWidget {
  const TaskListSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      height: _sizeScreen.hp(52),
      width: _sizeScreen.wp(60),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Order Berlangsung',
            style: poppinsw400sz20C1216C,
          ),
          SizedBox(
            height: _sizeScreen.hp(1),
          ),
          SizedBox(
            height: _sizeScreen.hp(8),
            child: Row(
              children: [
                Container(
                  width: _sizeScreen.wp(3),
                  height: _sizeScreen.hp(5),
                  decoration: BoxDecoration(
                      border: Border.all(color: Color(0XFFC4C4C4), width: 1),
                      borderRadius: BorderRadius.all(Radius.circular(40))),
                  child: Center(
                    child: IconButton(
                      icon: Icon(Icons.filter_alt_outlined),
                      color: Color(0xFE4F5262),
                      onPressed: () {
                        debugPrint('Clicked on Filter');
                      },
                    ),
                  ),
                ),
                SizedBox(
                  width: _sizeScreen.wp(0.75),
                ),
                Expanded(
                  child: ListView(
                    physics: BouncingScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    children: [
                      GroupButton(
                        buttonHeight: _sizeScreen.hp(5),
                        spacing: _sizeScreen.wp(0.75),
                        isRadio: true,
                        direction: Axis.horizontal,
                        onSelected: (index, isSelected) {
                          ///FIXME Find a way to make this function not using else if , is that possible ? i don't know rn
                          if (index == 0 && isSelected) {
                            Provider.of<TaskListNotifier>(context,
                                    listen: false)
                                .getAllData();
                          } else if (index == 1 && isSelected) {
                            ///TODO Data 2
                          } else if (index == 2 && isSelected) {
                            ///TODO Data 3
                          } else if (index == 3 && isSelected) {
                            ///TODO Data 4
                          } else if (index == 4 && isSelected) {
                            ///TODO Data 5
                          } else if (index == 5 && isSelected) {
                            ///TODO Data 6
                          } else if (index == 6 && isSelected) {
                            ///TOOO Data 7
                          }
                        },
                        buttons:
                            context.watch<HomeNotifier>().statusOrders.toList(),
                        selectedTextStyle: nunitoSansw700sz16,
                        unselectedTextStyle: nunitoSansw400sz16,
                        selectedColor: Color(0xFFEFFFF8),
                        unselectedColor: Colors.white,
                        selectedBorderColor: Color(0xFF28C27D),
                        unselectedBorderColor: Color(0xFFC7C8CD),
                        borderRadius: BorderRadius.circular(20.0),
                        selectedShadow: <BoxShadow>[
                          BoxShadow(color: Colors.white)
                        ],
                        unselectedShadow: <BoxShadow>[
                          BoxShadow(color: Colors.white)
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.only(
                  top: _sizeScreen.hp(1),
                  left: _sizeScreen.wp(1),
                  right: _sizeScreen.wp(1)),
              color: Color(0xFEF6F6F6),
              child: Column(
                children: [
                  Container(
                    height: _sizeScreen.hp(4),
                    child: Row(
                      children: [
                        Container(
                          child: Text(
                            'KONSUMEN',
                            style: nunitoSansw400sz12C12162CLt1,
                          ),
                          width: _sizeScreen.wp(11),
                        ),
                        Container(
                          width: _sizeScreen.wp(6),
                          child: Text(
                            'TIPE UNIT',
                            style: nunitoSansw400sz12C12162CLt1,
                          ),
                        ),
                        Container(
                          width: _sizeScreen.wp(8),
                          child: Text(
                            'TANGGAL ORDER',
                            style: nunitoSansw400sz12C12162CLt1,
                          ),
                        ),
                        SizedBox(
                          width: _sizeScreen.wp(1),
                        ),
                        Container(
                          width: _sizeScreen.wp(10),
                          child: Text(
                            'JADWAL SURVEY',
                            style: nunitoSansw400sz12C12162CLt1,
                          ),
                        ),
                        SizedBox(
                          width: _sizeScreen.wp(1),
                        ),
                        Container(
                          width: _sizeScreen.wp(7),
                          child: Text(
                            'STATUS',
                            style: nunitoSansw400sz12C12162CLt1,
                          ),
                        )
                      ],
                    ),
                  ),
                  Divider(
                    height: 1,
                  ),
                  SizedBox(
                    height: _sizeScreen.hp(1),
                  ),
                  Expanded(
                    child: ListView(
                      children: [
                        Consumer<TaskListNotifier>(
                            builder: (context, tasklist, _) {
                          return tasklist.data.length > 0
                              ? GroupedListView(
                                  order: GroupedListOrder.DESC,
                                  elements: tasklist.data,
                                  shrinkWrap: true,
                                  groupBy: (ModelTaskList element) =>
                                      formatDateddMMMyyyy(element.orderDate!),
                                  separator: SizedBox(
                                    height: 15,
                                  ),
                                  groupSeparatorBuilder: (String groupValue) =>
                                      Container(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 20),
                                    margin: EdgeInsets.symmetric(vertical: 20),
                                    child: Text(groupValue.toString() +
                                        ' (' +
                                        Provider.of<TaskListNotifier>(context,
                                                listen: false)
                                            .countOrderByDate(
                                                groupValue.toString())
                                            .toString() +
                                        ' Survey Data)'),
                                  ),
                                  itemBuilder: (BuildContext context,
                                      ModelTaskList modelTaskList) {
                                    return CustomCardTaskList(
                                        modelTaskList: modelTaskList);
                                  },
                                )
                              : EmptyPage(
                                  icon: Icons.article_outlined,
                                  message: 'No Have Tasklist',
                                  message1: "Have a Nice Day",
                                );
                        }),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
