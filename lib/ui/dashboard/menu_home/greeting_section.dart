part of 'home_page.dart';

class GreetingSection extends StatelessWidget {
  final String userName;
  final String job;

  const GreetingSection({Key? key, required this.userName, required this.job})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Row(
      children: [
        Container(
          width: _sizeScreen.wp(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: _sizeScreen.hp(5),
                width: _sizeScreen.wp(40),
                child: Text(
                  'Hai, $userName',
                  style: nunitoSansw400sz20,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Container(
                height: _sizeScreen.hp(5),
                width: _sizeScreen.wp(40),
                child: Text(
                  job,
                  style: nunitoSansw700sz16,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
        ),
        Spacer(),
        Container(
          child: WidgetIconNotifications(
            totalNotif: 2,
          ),
        )
      ],
    );
  }
}
