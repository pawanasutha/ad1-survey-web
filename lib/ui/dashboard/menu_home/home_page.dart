import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:group_button/group_button.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';
import 'package:survey_app/models/tasklist/model_tasklist.dart';
import 'package:survey_app/state_management/home_notifier.dart';
import 'package:survey_app/state_management/idm_notifier.dart';
import 'package:survey_app/state_management/tasklist_notifier.dart';
import 'package:survey_app/ui/dashboard/main_task/ide_pages/main_task_ide_page.dart';
import 'package:survey_app/utils/custom_widgets/custom_widgets.dart';
import 'package:survey_app/ui/footer_section.dart';
import 'package:survey_app/utils/utils.dart';
import 'package:provider/provider.dart';

part 'greeting_section.dart';

part 'transaction_header_home_section.dart';

part 'target_section.dart';

part 'tasklist_section.dart';

part 'detail_tasklist_section.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Flexible(
            flex: 3,
            child: Container(
              padding: EdgeInsets.only(
                  top: _sizeScreen.hp(3),
                  left: _sizeScreen.wp(2),
                  right: _sizeScreen.wp(2)),
              width: double.infinity,
              height: double.infinity,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    greetSurveyor(context),
                    TransactionHeaderHomeSection(),
                    SizedBox(
                      height: _sizeScreen.hp(4),
                    ),
                    TaskListSection(),
                  ]),
            ),
          ),
          Consumer<HomeNotifier>(
            builder: (context, home, _) {
              return Flexible(
                flex: 2,
                fit: FlexFit.tight,
                child: Container(
                    padding: EdgeInsets.only(
                        top: _sizeScreen.hp(3),
                        left: _sizeScreen.wp(2.5),
                        right: _sizeScreen.wp(3.5)),
                    color: Color(0xFFF6F7FB),
                    width: _sizeScreen.wp(40),
                    height: double.infinity,
                    child: home.isTaskListDataPreview == false
                        ? TargetSection()
                        : DetailTaskListSection()),
              );
            },
          ),
        ],
      ),
      bottomNavigationBar: FooterSection(),
    );
  }

  Widget greetSurveyor(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return FutureBuilder(
        future: context.watch<IDMNotifier>().getNameSurveyor(context),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            String userName = Provider.of<IDMNotifier>(context, listen: false)
                    .user
                    .objectName ??
                '-';
            /* String job =
                Provider.of<HomeNotifier>(context, listen: false).user.login!;*/
            String job = 'Surveyor';
            return GreetingSection(userName: userName, job: job);
          } else {
            return Shimmer.fromColors(
              child: Container(
                width: double.infinity,
                margin: EdgeInsets.only(
                    top: _sizeScreen.hp(3), left: _sizeScreen.wp(2)),
                child: Row(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: _sizeScreen.hp(3),
                          width: _sizeScreen.wp(20),
                          color: Colors.white,
                        ),
                        SizedBox(height: _sizeScreen.hp(1)),
                        Container(
                          height: _sizeScreen.hp(3),
                          width: _sizeScreen.wp(20),
                          color: Colors.white,
                        ),
                      ],
                    ),
                    Spacer(),
                    Container(
                      height: 24,
                      width: 24,
                      color: Colors.white,
                    )
                  ],
                ),
              ),
              baseColor: Colors.grey[300]!,
              highlightColor: Colors.grey[100]!,
            );
          }
        });
  }
}
