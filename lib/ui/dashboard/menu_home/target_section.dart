part of 'home_page.dart';

class TargetSection extends StatelessWidget {
  const TargetSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      margin: EdgeInsets.only(bottom: _sizeScreen.hp(40)),
      width: _sizeScreen.wp(18),
      height: _sizeScreen.hp(30),
      padding: EdgeInsets.symmetric(
          horizontal: _sizeScreen.wp(2), vertical: _sizeScreen.hp(5)),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Target April 2021',
                style: nunitoSansw600sz16,
              ),
            ],
          ),
          SizedBox(
            height: _sizeScreen.hp(3),
          ),
          Text(
            'Unit Finance',
            style: nunitoSansw400sz14,
          ),
          SizedBox(
            height: 16,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('16 Unit', style: nunitoSansw400sz14C28C27D),
              Text('27 Unit', style: nunitoSansw400sz14C4F5262)
            ],
          ),
          SizedBox(
            height: _sizeScreen.hp(2),
          ),
          LinearProgressIndicator(
            value: 0.7,
            backgroundColor: Color(0xFFF6F6F6),
            valueColor: AlwaysStoppedAnimation<Color>(Color(0xFE28C27D)),
          ),
          SizedBox(
            height: 8,
          ),
          Text('9 unit lagi untuk mencapai target!',
              style: nunitoSansw700sz12C92949D),
          SizedBox(
            height: _sizeScreen.hp(3),
          ),
          Text(
            'Amount Finance',
            style: nunitoSansw400sz14,
          ),
          SizedBox(
            height: 16,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Rp 154.560.000', style: nunitoSansw400sz14C28C27D),
              Text('Rp 350.000.000', style: nunitoSansw400sz14C4F5262)
            ],
          ),
          SizedBox(
            height: 8,
          ),
          LinearProgressIndicator(
            value: 0.5,
            backgroundColor: Color(0xFFF6F6F6),
            valueColor: AlwaysStoppedAnimation<Color>(Color(0xFE28C27D)),
          ),
          SizedBox(
            height: 8,
          ),
          Text('Rp 206.695.000 lagi untuk mencapai target!',
              style: nunitoSansw700sz12C92949D),
        ],
      ),
    );
  }
}
