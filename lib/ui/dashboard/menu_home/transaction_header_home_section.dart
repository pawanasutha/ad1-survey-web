part of 'home_page.dart';

class TransactionHeaderHomeSection extends StatelessWidget {
  final String currentMonth =
      '${listOfMonth[DateTime.now().month - 1]} ${DateTime.now().year}';
  TransactionHeaderHomeSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      child: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(top: _sizeScreen.hp(2.5)),
            height: _sizeScreen.hp(20),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(
                        'assets/images/image_background_header_home.png'),
                    fit: BoxFit.fill)),
          ),
          Container(
            height: _sizeScreen.hp(22.5),
            width: _sizeScreen.wp(15),
            margin: EdgeInsets.only(left: _sizeScreen.wp(32)),
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(
                      "assets/images/image_background_header_person_home.png"),
                  fit: BoxFit.fill),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
                left: _sizeScreen.wp(1.5), top: _sizeScreen.hp(5)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Transaksi $currentMonth',
                    style: nunitoSansw400sz20CWhite),
                SizedBox(
                  height: _sizeScreen.hp(3),
                ),
                Container(
                  child: Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          border: Border.all(color: Color(0xFEF6F6F6)),
                          color: Colors.white,
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              height: _sizeScreen.hp(4),
                              width: _sizeScreen.wp(3),
                              decoration: BoxDecoration(
                                color: Color(0xfeF6F6F6),
                                shape: BoxShape.circle,
                              ),
                              child: Center(
                                  child: Image.asset(
                                      'assets/icons/icon_order_masuk.png')),
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    width: _sizeScreen.wp(5),
                                    child: Text(
                                      'Order Masuk',
                                      style: nunitoSansw400sz14C92949D,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                  Container(
                                    width: _sizeScreen.wp(5),
                                    child: Text(
                                      'TODO Order Masuk',
                                      style: nunitoSansw700sz14,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        width: _sizeScreen.wp(12),
                        height: _sizeScreen.hp(8),
                      ),
                      SizedBox(
                        width: _sizeScreen.wp(1.5),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          border: Border.all(color: Color(0xFEF6F6F6)),
                          color: Colors.white,
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              height: _sizeScreen.hp(4),
                              width: _sizeScreen.wp(3),
                              decoration: BoxDecoration(
                                color: Color(0xfeF6F6F6),
                                shape: BoxShape.circle,
                              ),
                              child: Center(
                                child: Icon(
                                  Icons.check,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    width: _sizeScreen.wp(5),
                                    child: Text(
                                      'PPD',
                                      style: nunitoSansw400sz14C92949D,
                                    ),
                                  ),
                                  Container(
                                    width: _sizeScreen.wp(5),
                                    child: Text(
                                      'TODO PPD',
                                      style: nunitoSansw700sz14,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        width: _sizeScreen.wp(12),
                        height: _sizeScreen.hp(8),
                      ),
                      SizedBox(
                        width: _sizeScreen.wp(1.5),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          border: Border.all(color: Color(0xFEF6F6F6)),
                          color: Colors.white,
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              height: _sizeScreen.hp(4),
                              width: _sizeScreen.wp(3),
                              decoration: BoxDecoration(
                                color: Color(0xfeF6F6F6),
                                shape: BoxShape.circle,
                              ),
                              child: Center(
                                child: Icon(
                                  Icons.star_border_outlined,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    width: _sizeScreen.wp(5),
                                    child: Text(
                                      'FID',
                                      style: nunitoSansw400sz14C92949D,
                                    ),
                                  ),
                                  Container(
                                    width: _sizeScreen.wp(5),
                                    child: Text(
                                      'TODO FID',
                                      style: nunitoSansw700sz14,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                        width: _sizeScreen.wp(12),
                        height: _sizeScreen.hp(8),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
