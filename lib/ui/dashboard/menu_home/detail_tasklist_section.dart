part of 'home_page.dart';

class DetailTaskListSection extends StatelessWidget {
  const DetailTaskListSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Consumer<HomeNotifier>(builder: (context, home, _) {
      return Container(
        width: _sizeScreen.wp(18),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(6),
          ),
        ),
        padding: EdgeInsets.symmetric(vertical: _sizeScreen.hp(2)),
        margin: EdgeInsets.only(bottom: _sizeScreen.hp(2)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ///HEADER
            Container(
              padding: EdgeInsets.symmetric(horizontal: _sizeScreen.wp(2)),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Column(
                    children: [
                      Text(
                        'Detail Order',
                        style: poppinsw600sz20,
                      ),
                      Text(
                        'ID 1827 827 2721',
                        style: nunitoSansw400sz14,
                      )
                    ],
                  ),
                  Spacer(),
                  InkWell(
                    child: Icon(
                      Icons.close,
                      color: Colors.black,
                    ),
                    onTap: () => home.isTaskListDataPreview = false,
                  )
                ],
              ),
            ),
            SizedBox(
              height: _sizeScreen.hp(1),
            ),

            ///Content
            Expanded(
              child: ListView(
                padding: EdgeInsets.symmetric(horizontal: _sizeScreen.wp(2)),
                children: [
                  ///Status
                  Container(
                    height: _sizeScreen.hp(5),
                    decoration: BoxDecoration(
                      color: Color(0xFFF6F9FE),
                      borderRadius: BorderRadius.all(
                        Radius.circular(6),
                      ),
                    ),
                    child: Center(
                      child: Text(
                        'Menunggu Survey',
                        style: nunitoSansw600sz16C0A66C2,
                      ),
                    ),
                  ),

                  SizedBox(
                    height: _sizeScreen.hp(1),
                  ),

                  ///QuickLookDataKonsumen
                  Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: _sizeScreen.wp(2),
                        vertical: _sizeScreen.hp(4)),
                    decoration: BoxDecoration(
                      color: Color(0xFFF6F6F6),
                      borderRadius: BorderRadius.all(
                        Radius.circular(6),
                      ),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              'Jaya Suparno',
                              style: poppinsw600sz18,
                            ),
                            Spacer(),
                            TextButton(
                              child: Text(
                                'Lihat detail',
                                overflow: TextOverflow.ellipsis,
                              ),
                              onPressed: () {
                                debugPrint('TODO Lihat Detail');
                                debugPrint(
                                    'Height ${_sizeScreen.hp(5)} Width ${_sizeScreen.wp(10)}');
                              },
                            )
                          ],
                        ),
                        Text(
                          'Honda Vario 150 Exclusive',
                          style: nunitoSansw400sz14,
                        ),
                        SizedBox(
                          height: _sizeScreen.hp(1),
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Asal Order',
                                style: nunitoSansw400sz14C92949D,
                              ),
                              Text(
                                'Dealer Trio Motor Perintis',
                                style: nunitoSansw400sz14,
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: _sizeScreen.hp(1),
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Tipe Survey',
                                style: nunitoSansw400sz14C92949D,
                              ),
                              Text(
                                'Regular Survey',
                                style: nunitoSansw400sz14,
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: _sizeScreen.hp(2),
                        ),
                        CustomButton(
                          textOnButton: 'WhatsApp Konsumen',
                          function: () =>
                              debugPrint('Clicked on WhatsApp Konsumen'),
                          backgroundButtonColor: Colors.transparent,
                          textStyle: nunitoSansw600sz14C0A66C2,
                        )
                      ],
                    ),
                  ),

                  SizedBox(
                    height: _sizeScreen.hp(1.5),
                  ),

                  ///InformasiSurvey
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              'Informasi Survey',
                              style: nunitoSansw700sz16,
                            ),
                            Spacer(),
                            TextButton(
                              child: Text(
                                'Ubah Jadwal',
                                overflow: TextOverflow.ellipsis,
                              ),
                              onPressed: () {
                                debugPrint('TODO Ubah Jadwal');
                              },
                            )
                          ],
                        ),
                        Divider(
                          color: Color(0xFFC7C8CD),
                        ),
                        SizedBox(
                          height: _sizeScreen.hp(1),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Sales Officer',
                                    style: nunitoSansw400sz14C92949D,
                                  ),
                                  Text(
                                    'Andre Hehanusa',
                                    style: nunitoSansw400sz14,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              height: _sizeScreen.hp(5),
                              width: _sizeScreen.wp(10),
                              child: OutlinedButton(
                                style: ButtonStyle(
                                  side: MaterialStateProperty.all<BorderSide>(
                                      BorderSide(color: Colors.lightBlue)),
                                  backgroundColor:
                                      MaterialStateProperty.resolveWith<Color>(
                                          (Set<MaterialState> states) {
                                    if (states.contains(MaterialState.pressed))
                                      return Colors.lightBlueAccent;
                                    return Colors.white;
                                  }),
                                  shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(22.0),
                                    ),
                                  ),
                                ),
                                onPressed: () =>
                                    debugPrint('Clicked on Icon Chat Sales'),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Image.asset(
                                      'assets/icons/icon_sms.png',
                                      color: Color(0xFF0A66C2),
                                    ),
                                    Text(
                                      'Chat Sales',
                                      style: nunitoSansw600sz14C0A66C2,
                                      overflow: TextOverflow.ellipsis,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: _sizeScreen.hp(1),
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Tanggal Survey',
                                style: nunitoSansw400sz14C92949D,
                              ),
                              Text(
                                'Kamis, 4 Maret 2021 ',
                                style: nunitoSansw400sz14,
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: _sizeScreen.hp(1),
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Jam Dilaksanakannya Survey',
                                style: nunitoSansw400sz14C92949D,
                              ),
                              Text(
                                '14.00-15.00 WIB',
                                style: nunitoSansw400sz14,
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: _sizeScreen.hp(1),
                        ),
                        Container(
                          height: _sizeScreen.hp(5),
                          decoration: BoxDecoration(
                            color: Color(0xFFF6F9FE),
                            borderRadius: BorderRadius.all(
                              Radius.circular(6),
                            ),
                          ),
                          child: Center(
                            child: Text(
                              'Estimasi waktu perjalanan 60 menit',
                              style: nunitoSansw400sz14,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: _sizeScreen.hp(1),
                        ),

                        ///Lokasi Survey
                        Text("Lokasi Survey", style: nunitoSansw700sz16),
                        SizedBox(
                          height: _sizeScreen.hp(1),
                        ),
                        GestureDetector(
                          onTap: () => debugPrint('TODO Goto GoogleMaps'),
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                vertical: _sizeScreen.hp(1),
                                horizontal: _sizeScreen.wp(1)),
                            width: double.infinity,
                            decoration: BoxDecoration(
                                border: Border.all(color: Color(0xFEC7C8CD)),
                                borderRadius: BorderRadius.circular(8),
                                color: Colors.transparent),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                    height: 51.75,
                                    width: 46.5,
                                    child: Image.asset(
                                      'assets/images/image_dummy_location.png',
                                    )),
                                Expanded(
                                  child: Container(
                                    width: _sizeScreen.wp(7),
                                    margin: EdgeInsets.symmetric(
                                        horizontal: _sizeScreen.wp(1.5)),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          'Jl Kemanggisan No.4',
                                          style: nunitoSansw700sz14,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        Text(
                                          'Palmerah, Jakarta Barat ...',
                                          style: nunitoSansw400sz12C92949D,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Icon(Icons.location_on)
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: _sizeScreen.hp(1),
                        ),

                        ///Warning
                        Container(
                          height: _sizeScreen.hp(8),
                          width: double.infinity,
                          decoration: BoxDecoration(
                              border: Border.all(color: Color(0xFEF9CA02)),
                              borderRadius: BorderRadius.circular(5),
                              color: Color(0xFEFEF5CB)),
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: _sizeScreen.wp(1),
                                vertical: _sizeScreen.hp(1)),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Icon(
                                  Icons.info,
                                  color: Color(0xFEF2B705),
                                ),
                                SizedBox(
                                  width: _sizeScreen.wp(1),
                                ),
                                Expanded(
                                  child: Text(
                                    'Pastikan Anda datang tepat waktu. Klik tombol dibawah untuk memulai survey & check in secara otomatis',
                                    style: nunitoSansw400sz12C12162C,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),

            ///Button
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: _sizeScreen.wp(2)),
                child: Column(
                  children: [
                    SizedBox(
                      height: _sizeScreen.hp(1),
                    ),
                    CustomButton(
                        backgroundButtonColor: Color(0xFFFFDD00),
                        textStyle: nunitoSansw700sz16,
                        textOnButton: 'Mulai Survey',
                        function: () => Get.to(() => MainTaskIDEPage())),
                    SizedBox(
                      height: _sizeScreen.hp(2),
                    ),
                    CustomButton(
                      backgroundButtonColor: Colors.white,
                      textStyle: nunitoSansw600sz16C0A66C2,
                      textOnButton: 'Batalkan Survey',
                      function: () =>
                          debugPrint('Clicked on Button Batalkan Survey'),
                    ),
                    SizedBox(
                      height: _sizeScreen.hp(1),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      );
    });
  }
}
