part of 'main_task_ide_page.dart';

class MainTaskIDESideSection extends StatefulWidget {
  const MainTaskIDESideSection({Key? key}) : super(key: key);

  @override
  _MainTaskIDESideSectionState createState() => _MainTaskIDESideSectionState();
}

class _MainTaskIDESideSectionState extends State<MainTaskIDESideSection> {
  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);

    Widget quickCustomerInformation() {
      return Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 48,
              width: 48,
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: Color(0xFFF3C451)),
              child: Center(
                child: Text(
                  'JS',
                  style: nunitoSansw700sz16ccWhite,
                ),
              ),
            ),
            SizedBox(
              width: _sizeScreen.wp(1),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Jaya Suparno',
                  style: poppinsw600sz20,
                ),
                SizedBox(
                  width: _sizeScreen.hp(1),
                ),
                Text('Honda Vario 150')
              ],
            )
          ],
        ),
      );
    }

    return Padding(
      padding: EdgeInsets.symmetric(
          vertical: _sizeScreen.hp(2), horizontal: _sizeScreen.wp(1.5)),
      child: Column(
        children: [
          quickCustomerInformation(),
          SizedBox(
            height: _sizeScreen.hp(1.5),
          ),
          Divider(
            color: Color(0xFFC7C8CD).withOpacity(0.5),
          ),
          SizedBox(
            height: _sizeScreen.hp(1.5),
          ),
          Container(
            child: Row(
              children: [
                CustomLeadingContainer(
                  borderColor: Color(0xffffff),
                  iconCenter: Image.asset(
                    'assets/icons/icons_ide/icon_data_konsumen.png',
                    color: Color(0xFF28C27D),
                  ),
                ),
                Text(
                  'Data Konsumen',
                  style: nunitoSansw700sz16C28C27D,
                )
              ],
            ),
          ),
          SizedBox(
            height: _sizeScreen.hp(1.5),
          ),
          Container(
            child: Row(
              children: [
                CustomLeadingContainer(
                  borderColor: Color(0xffffff),
                  iconCenter: Image.asset(
                    'assets/icons/icons_ide/icon_data_penjamin.png',
                    color: Color(0xFF92949D),
                  ),
                ),
                Text(
                  'Data Penjamin',
                  style: nunitoSansw400sz16C92949D,
                )
              ],
            ),
          ),
          SizedBox(
            height: _sizeScreen.hp(1.5),
          ),
          Container(
            child: Row(
              children: [
                CustomLeadingContainer(
                  borderColor: Color(0xffffff),
                  iconCenter: Image.asset(
                    'assets/icons/icons_ide/icon_unit_jaminan.png',
                    color: Color(0xFF92949D),
                  ),
                ),
                Text(
                  'Unit & Jaminan',
                  style: nunitoSansw400sz16C92949D,
                )
              ],
            ),
          ),
          SizedBox(
            height: _sizeScreen.hp(1.5),
          ),
          Container(
            child: Row(
              children: [
                CustomLeadingContainer(
                  borderColor: Color(0xffffff),
                  iconCenter: Image.asset(
                    'assets/icons/icons_ide/icon_pembiayaan.png',
                    color: Color(0xFF92949D),
                  ),
                ),
                Text(
                  'Pembiayaan',
                  style: nunitoSansw400sz16C92949D,
                )
              ],
            ),
          ),
          SizedBox(
            height: _sizeScreen.hp(1.5),
          ),
          Container(
            child: Row(
              children: [
                CustomLeadingContainer(
                  borderColor: Color(0xffffff),
                  iconCenter: Image.asset(
                    'assets/icons/icons_ide/icon_job_income.png',
                    color: Color(0xFF92949D),
                  ),
                ),
                Text(
                  'Pekerjaan & Pendapatan',
                  style: nunitoSansw400sz16C92949D,
                )
              ],
            ),
          ),
          SizedBox(
            height: _sizeScreen.hp(1.5),
          ),
          Container(
            child: Row(
              children: [
                CustomLeadingContainer(
                  borderColor: Color(0xffffff),
                  iconCenter: Image.asset(
                    'assets/icons/icons_ide/icon_sales_information.png',
                    color: Color(0xFF92949D),
                  ),
                ),
                Text(
                  'Informasi Sales Jaminan',
                  style: nunitoSansw400sz16C92949D,
                )
              ],
            ),
          ),
          SizedBox(
            height: _sizeScreen.hp(1.5),
          ),
          Container(
            child: Row(
              children: [
                CustomLeadingContainer(
                  borderColor: Color(0xffffff),
                  iconCenter: Image.asset(
                    'assets/icons/icon_document.png',
                    color: Color(0xFF92949D),
                  ),
                ),
                Text(
                  'Dokumen',
                  style: nunitoSansw400sz16C92949D,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
