import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:survey_app/ui/dashboard/main_task/ide_pages/ide_consumer_pages/ide_consumer_pages.dart';
import 'package:survey_app/ui/dashboard/main_task/survey_pages/main_task_sre_page.dart';
import 'package:survey_app/utils/custom_widgets/custom_widgets.dart';
import 'package:survey_app/utils/utils.dart';

import '../../../footer_section.dart';

part 'main_task_ide_side_section.dart';

part 'main_task_ide_section.dart';

class MainTaskIDEPage extends StatelessWidget {
  const MainTaskIDEPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.symmetric(
                  vertical: _sizeScreen.hp(3), horizontal: _sizeScreen.wp(3)),
              child: Image(
                image:
                    AssetImage('assets/icons/icon_ADMF_background_putih.png'),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: _sizeScreen.wp(3)),
              height: _sizeScreen.hp(7),
              color: Color(0xFFF6F6F6),
              child: Row(
                children: [
                  Container(
                    child: Text(
                      'Beranda',
                      style: nunitoSansw400sz14,
                    ),
                  ),
                  SizedBox(
                    width: _sizeScreen.wp(0.5),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    size: _sizeScreen.hp(2),
                  ),
                  SizedBox(
                    width: _sizeScreen.wp(0.5),
                  ),
                  Container(
                    child: Text(
                      'Detail Order',
                      style: nunitoSansw400sz14,
                    ),
                  ),
                  SizedBox(
                    width: _sizeScreen.wp(0.5),
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    size: _sizeScreen.hp(2),
                  ),
                  SizedBox(
                    width: _sizeScreen.wp(0.5),
                  ),
                  Container(
                    child: Text(
                      'Review IDE',
                      style: nunitoSansw700sz14,
                    ),
                  ),
                ],
              ),
            ),
            Container(
                child: Column(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(
                      vertical: _sizeScreen.wp(1),
                      horizontal: _sizeScreen.wp(8)),
                  child: Row(
                    children: [
                      Container(
                        child: Row(
                          children: [
                            IconButton(
                              icon: Icon(Icons.arrow_back_outlined),
                              onPressed: () => Get.back(),
                            ),
                            SizedBox(
                              width: _sizeScreen.wp(0.5),
                            ),
                            Text(
                              'Review Data IDE',
                              style: poppinsw600sz20,
                            )
                          ],
                        ),
                      ),
                      Spacer(),
                      CustomButton(
                          width: _sizeScreen.wp(15),
                          borderSideColor: Colors.transparent,
                          textOnButton: 'Lanjut Pengisian SRE',
                          textStyle: nunitoSansw700sz16,
                          backgroundButtonColor: Color(0xFFFFDD00),
                          function: () => Get.to(() => MainTaskSREPage()))
                    ],
                  ),
                ),
                MainTaskIDESection()
              ],
            )),
          ],
        ),
        bottomNavigationBar: FooterSection());
  }
}
