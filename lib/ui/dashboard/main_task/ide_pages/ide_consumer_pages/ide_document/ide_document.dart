part of '../ide_consumer_pages.dart';

class IDEDocument extends StatelessWidget {
  const IDEDocument({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Material(
      child: Theme(
        data: Theme.of(context).copyWith(accentColor: Colors.black),
        child: ExpansionTile(
          collapsedBackgroundColor: Colors.white,
          backgroundColor: Colors.white,
          leading: CustomLeadingContainer(
              borderColor: Colors.yellowAccent,
              iconCenter: Image.asset(
                'assets/icons/icon_document.png',
                color: Color(0xFF92949D),
              )),
          subtitle: Text(
            'Harap Lengkapi data!',
            style: nunitoSansw400sz12C92949D,
          ),
          title: Text(
            'Dokumen',
            style: nunitoSansw700sz16,
          ),
          children: [
            ///TODO
            for (int i = 0; i < 10; i++) Text('TODO Document')
          ],
        ),
      ),
    );
  }
}
