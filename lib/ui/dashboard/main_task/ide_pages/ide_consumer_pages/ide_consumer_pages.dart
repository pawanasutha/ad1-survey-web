import 'package:flutter/material.dart';
import 'package:survey_app/utils/custom_widgets/custom_widgets.dart';
import 'package:survey_app/utils/utils.dart';

part 'ide_konsumen_data/ide_konsumen_data.dart';

part 'ide_guarantor/ide_guarantor.dart';

part 'ide_financing/ide_financing.dart';

part 'ide_unit_and_guarantee/ide_unit_and_guarantee.dart';

part 'ide_job_and_income/ide_job_and_income.dart';

part 'ide_sales_information/ide_sales_information.dart';

part 'ide_document/ide_document.dart';

class IdeConsumerPages extends StatefulWidget {
  const IdeConsumerPages({Key? key}) : super(key: key);

  @override
  _IdeConsumerPagesState createState() => _IdeConsumerPagesState();
}

class _IdeConsumerPagesState extends State<IdeConsumerPages> {
  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return ListView(
      children: [
        Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 2),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: IDEKonsumenData()),
        SizedBox(
          height: _sizeScreen.hp(1),
        ),
        Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 2),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: IDEGuarantor()),
        SizedBox(
          height: _sizeScreen.hp(1),
        ),
        Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 2),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: IDEUnitAndGuarantee()),
        SizedBox(
          height: _sizeScreen.hp(1),
        ),
        Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 2),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: IDEFinancing()),
        SizedBox(
          height: _sizeScreen.hp(1),
        ),
        Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 2),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: IDEJobAndIncome()),
        SizedBox(
          height: _sizeScreen.hp(1),
        ),
        Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 2),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: IDESalesInformation()),
        SizedBox(
          height: _sizeScreen.hp(1),
        ),
        Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 2),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: IDEDocument()),
      ],
    );
  }
}
