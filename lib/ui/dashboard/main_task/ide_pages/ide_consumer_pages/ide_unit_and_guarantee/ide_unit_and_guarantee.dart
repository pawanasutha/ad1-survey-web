part of '../ide_consumer_pages.dart';

class IDEUnitAndGuarantee extends StatelessWidget {
  const IDEUnitAndGuarantee({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Material(
      child: Theme(
        data: Theme.of(context).copyWith(accentColor: Colors.black),
        child: ExpansionTile(
          collapsedBackgroundColor: Colors.white,
          backgroundColor: Colors.white,
          leading: CustomLeadingContainer(
            borderColor: Color(0xFE28C27D),
            iconCenter: Icon(
              Icons.check,
              color: Color(0xFE28C27D),
            ),
          ),
          title: Text(
            'Unit dan Jaminan',
            style: nunitoSansw700sz16,
          ),
          children: [
            ///TODO
            for (int i = 0; i < 10; i++) Text('TODO UNIT & GUARANTEE')
          ],
        ),
      ),
    );
  }
}
