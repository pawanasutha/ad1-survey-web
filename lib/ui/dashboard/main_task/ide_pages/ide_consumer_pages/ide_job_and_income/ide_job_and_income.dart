part of '../ide_consumer_pages.dart';

class IDEJobAndIncome extends StatelessWidget {
  const IDEJobAndIncome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Material(
      child: Theme(
        data: Theme.of(context).copyWith(accentColor: Colors.black),
        child: ExpansionTile(
          collapsedBackgroundColor: Colors.white,
          backgroundColor: Colors.white,
          leading: CustomLeadingContainer(
              borderColor: Colors.yellowAccent,
              iconCenter: Image.asset(
                'assets/icons/icons_ide/icon_job_income.png',
                color: Color(0xFF92949D),
              )),
          subtitle: Text(
            'Harap Lengkapi data!',
            style: nunitoSansw400sz12C92949D,
          ),
          title: Text(
            'Pekerjaan dan Pendapatan',
            style: nunitoSansw700sz16,
          ),
          children: [
            ///TODO
            for (int i = 0; i < 10; i++) Text('TODO JOB_INCOME')
          ],
        ),
      ),
    );
  }
}
