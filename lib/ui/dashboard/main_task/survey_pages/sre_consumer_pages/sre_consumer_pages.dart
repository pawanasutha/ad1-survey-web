import 'package:flutter/material.dart';
import 'package:survey_app/utils/custom_widgets/custom_widgets.dart';
import 'package:survey_app/utils/utils.dart';

part 'survey_of_photos_activity/survey_of_photos_activity.dart';
part 'survey_assets_information/survey_assets_information.dart';
part 'survey_environmentals_information/survey_environmentals_information.dart';

class SREConsumerPages extends StatelessWidget {
  const SREConsumerPages({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return ListView(
      children: [
        Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 2),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: SurveyOfPhotosActivity()),
        SizedBox(
          height: _sizeScreen.hp(1),
        ),
        Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 2),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: SurveyAssetsInformation()),
        SizedBox(
          height: _sizeScreen.hp(1),
        ),
        Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 2),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: SurveyEnvironmentalsInformation()),
      ],
    );
  }
}
