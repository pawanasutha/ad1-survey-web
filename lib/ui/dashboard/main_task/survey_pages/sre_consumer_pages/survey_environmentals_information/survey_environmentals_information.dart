part of '../sre_consumer_pages.dart';

class SurveyEnvironmentalsInformation extends StatelessWidget {
  const SurveyEnvironmentalsInformation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Material(
      child: Theme(
        data: Theme.of(context).copyWith(accentColor: Colors.black),
        child: ExpansionTile(
          collapsedBackgroundColor: Colors.white,
          backgroundColor: Colors.white,
          leading: CustomLeadingContainer(
              borderColor: Color(0xFFF6F6F6),
              iconCenter: Image.asset(
                'assets/icons/icon_document.png',
                color: Color(0xFFC7C8CD),
              )),
          title: Text(
            'Informasi Lingkungan',
            style: nunitoSansw700sz16,
          ),
          children: [
            ///TODO
            for (int i = 0; i < 10; i++) Text('TODO Informasi Lingkungan')
          ],
        ),
      ),
    );
  }
}
