part of 'main_task_sre_page.dart';

class MainTaskSRESection extends StatelessWidget {
  const MainTaskSRESection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: _sizeScreen.wp(7)),
      height: _sizeScreen.hp(65),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: _sizeScreen.wp(20),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(32),
                    bottomLeft: Radius.circular(32))),
            child: MainTaskSRESideSection(),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(
                  horizontal: _sizeScreen.wp(3), vertical: _sizeScreen.hp(4)),
              decoration: BoxDecoration(
                  color: Color(0xFFF6F7FB),
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(32),
                      bottomRight: Radius.circular(32))),
              child: SREConsumerPages(),
            ),
          )
        ],
      ),
    );
  }
}
