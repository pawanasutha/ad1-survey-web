import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:survey_app/state_management/idm_notifier.dart';
import 'package:survey_app/utils/utils.dart';

import '../../footer_section.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key? key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          ElevatedButton(
            onPressed: () {
              Provider.of<IDMNotifier>(context, listen: false).logout(context);
            },
            child: Text('Keluar Dari Aplikasi', style: nunitoSansw400sz16),
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24),
                side: BorderSide(color: Colors.yellow),
              ),
              primary: Colors.white,
            ),
          ),
          Center(
            child: Text('Settings Page'),
          ),
          for (int i = 0; i < 100; i++) Text('asd'),
          Text('the end')
        ],
      ),
      bottomNavigationBar: FooterSection(),
    );
  }
}
