import 'package:flutter/material.dart';
import 'package:popover/popover.dart';
import 'package:provider/provider.dart';
import 'package:survey_app/state_management/home_notifier.dart';
import 'package:survey_app/ui/dashboard/menu_asuransi/insurance_page.dart';
import 'package:survey_app/ui/dashboard/menu_home/home_page.dart';
import 'package:survey_app/ui/dashboard/menu_pengaturan/settings_page.dart';
import 'package:survey_app/ui/dashboard/menu_profile/profile_page.dart';
import 'package:survey_app/ui/dashboard/menu_status_order/status_order_page.dart';
import 'package:survey_app/ui/settings_section.dart';
import 'package:survey_app/utils/custom_widgets/custom_widgets.dart';
import 'package:survey_app/utils/utils.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  int _indexNavigation = 0;

  void onTabTapped(int newIndex) {
    setState(() {
      _indexNavigation = newIndex;
    });
  }

  final menuTabs = [
    HomePage(),
    StatusOrderPage(),
    InsurancePage(),
    ProfilePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Row(
        children: <Widget>[
          sideMenu(context),
          VerticalDivider(thickness: 1, width: 1),
          // This is the main content.
          Expanded(
            child: menuTabs[_indexNavigation],
          )
        ],
      ),
    );
  }

  Widget sideMenu(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      width: _sizeScreen.wp(15.5),
      child: Column(
        children: [
          Flexible(
            child: NavigationRail(
              trailing: Column(
                children: [
                  SizedBox(
                    height: _sizeScreen.hp(15),
                  ),
                  SettingTrailing()
                ],
              ),
              backgroundColor: Color(0xFE39225A),
              leading: Padding(
                padding: EdgeInsets.only(top: _sizeScreen.hp(2)),
                child: Image(
                  image: AssetImage('assets/icons/icon_adira.png'),
                ),
              ),
              selectedIndex: _indexNavigation,
              onDestinationSelected: (newIndexNavigation) =>
                  onTabTapped(newIndexNavigation),
              labelType: NavigationRailLabelType.selected,
              destinations: <NavigationRailDestination>[
                NavigationRailDestination(
                    icon: Container(
                      margin: EdgeInsets.only(
                          left: _sizeScreen.wp(3), top: _sizeScreen.hp(12)),
                      child: Row(
                        children: [
                          Image.asset(
                              'assets/icons/icons_navigation/icon_home.png'),
                          SizedBox(
                            width: _sizeScreen.wp(2),
                          ),
                          Text(
                            'Beranda',
                            style: nunitoSansw700sz14ccwhite,
                          )
                        ],
                      ),
                    ),
                    selectedIcon: Container(
                      margin: EdgeInsets.only(
                          left: _sizeScreen.wp(3), top: _sizeScreen.hp(12)),
                      child: Row(
                        children: [
                          Image.asset(
                              'assets/icons/icons_navigation/icon_home_active.png'),
                          SizedBox(
                            width: _sizeScreen.wp(2),
                          ),
                          Text(
                            'Beranda',
                            style: nunitoSansw700sz14ccwhite,
                          )
                        ],
                      ),
                    ),
                    label: Text('')),
                NavigationRailDestination(
                    icon: Container(
                      margin: EdgeInsets.only(left: _sizeScreen.wp(3)),
                      child: Row(
                        children: [
                          Image.asset(
                              'assets/icons/icons_navigation/icon_statusorder.png'),
                          SizedBox(
                            width: _sizeScreen.wp(2),
                          ),
                          Container(
                            width: _sizeScreen.wp(5),
                            child: Text(
                              'Status Order',
                              style: nunitoSansw700sz14ccwhite,
                              overflow: TextOverflow.ellipsis,
                            ),
                          )
                        ],
                      ),
                    ),
                    selectedIcon: Container(
                      margin: EdgeInsets.only(left: _sizeScreen.wp(3)),
                      child: Row(
                        children: [
                          Image.asset(
                              'assets/icons/icons_navigation/icon_statusorder_active.png'),
                          SizedBox(
                            width: _sizeScreen.wp(2),
                          ),
                          Text(
                            'Status Order',
                            style: nunitoSansw700sz14ccwhite,
                          )
                        ],
                      ),
                    ),
                    label: Text('')),
                NavigationRailDestination(
                    icon: Container(
                      margin: EdgeInsets.only(left: _sizeScreen.wp(3)),
                      child: Row(
                        children: [
                          Image.asset(
                              'assets/icons/icons_navigation/icon_insurance.png'),
                          SizedBox(
                            width: _sizeScreen.wp(2),
                          ),
                          Text(
                            'Asuransi',
                            style: nunitoSansw700sz14ccwhite,
                          )
                        ],
                      ),
                    ),
                    selectedIcon: Container(
                      margin: EdgeInsets.only(left: _sizeScreen.wp(3)),
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/icons/icons_navigation/icon_insurance.png',
                            color: Colors.yellow,
                          ),
                          SizedBox(
                            width: _sizeScreen.wp(2),
                          ),
                          Text(
                            'Asuransi',
                            style: nunitoSansw700sz14ccwhite,
                          )
                        ],
                      ),
                    ),
                    label: Text('')),
                NavigationRailDestination(
                    padding: EdgeInsets.only(bottom: _sizeScreen.hp(5)),
                    icon: Container(
                      margin: EdgeInsets.only(left: _sizeScreen.wp(3)),
                      child: Row(
                        children: [
                          Image.asset(
                              'assets/icons/icons_navigation/icon_profile.png'),
                          SizedBox(
                            width: _sizeScreen.wp(2),
                          ),
                          Text(
                            'Profile',
                            style: nunitoSansw700sz14ccwhite,
                          )
                        ],
                      ),
                    ),
                    selectedIcon: Container(
                      margin: EdgeInsets.only(left: _sizeScreen.wp(3)),
                      child: Row(
                        children: [
                          Image.asset(
                              'assets/icons/icons_navigation/icon_profile_active.png'),
                          SizedBox(
                            width: _sizeScreen.wp(2),
                          ),
                          Text(
                            'Profile',
                            style: nunitoSansw700sz14ccwhite,
                          )
                        ],
                      ),
                    ),
                    label: Text('')),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
