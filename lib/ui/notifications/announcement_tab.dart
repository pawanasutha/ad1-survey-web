part of 'notifications_page.dart';

class AnnouncementTab extends StatelessWidget {
  const AnnouncementTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<AnnouncementNotifier>(
      builder: (context, announcement, _) {
        return RefreshIndicator(
            onRefresh: () async {
              debugPrint('Refresh');
            },
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              /* physics: NeverScrollableScrollPhysics(),*/
              itemCount: announcement.modelNotifications.isEmpty
                  ? 1
                  : announcement.modelNotifications.length,
              itemBuilder: (BuildContext context, int index) {
                if (announcement.modelNotifications.isEmpty)
                  return EmptyPage(
                    icon: Icons.announcement_outlined,
                    message: 'No Have Announcement',
                    message1: "Have a Nice Day",
                  );
                return CustomCardPreviewAnnouncement(
                  modelAnnouncement: announcement.modelNotifications[index],
                );
              },
            ));
      },
    );
  }
}
