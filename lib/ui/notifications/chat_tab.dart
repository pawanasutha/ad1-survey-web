part of 'notifications_page.dart';

class ChatTab extends StatelessWidget {
  const ChatTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ChatNotifier>(builder: (context, chat, _) {
      return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        /*physics: NeverScrollableScrollPhysics(),*/
        itemCount: chat.modelChat.isEmpty ? 1 : chat.modelChat.length,
        itemBuilder: (BuildContext context, int index) {
          if (chat.modelChat.isEmpty)
            return EmptyPage(
              icon: Icons.chat,
              message: 'You Have No Chat',
              message1: "Have a Nice Day",
            );
          return CustomCardPreviewChat(
            modelChat: chat.modelChat[index],
          );
        },
      );
    });
  }
}
