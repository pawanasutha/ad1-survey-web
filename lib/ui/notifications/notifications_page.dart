import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:survey_app/state_management/notifications_notifier/notifications_notifier.dart';
import 'package:survey_app/utils/custom_widgets/custom_widgets.dart';
import 'package:survey_app/utils/utils.dart';

part 'announcement_tab.dart';

part 'chat_tab.dart';

class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage>
    with SingleTickerProviderStateMixin {
  static const List<Tab> myTabs = <Tab>[
    Tab(text: 'Pemberitahuan'),
    Tab(text: 'Chat'),
  ];

  int _selectedIndex = 0;
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: myTabs.length);
    _tabController!.addListener(() {
      setState(() {
        _selectedIndex = _tabController!.index;
      });
    });
  }

  String getTittle(int index) {
    const start = "Tab(text: \")";
    const end = "\")";

    final startIndex = myTabs[index].toString().indexOf(start);
    final endIndex =
        myTabs[index].toString().indexOf(end, startIndex + start.length);

    return myTabs[index]
        .toString()
        .substring(startIndex + start.length, endIndex);
  }

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor:
            Colors.white, //Changing this will change the color of the TabBar
        accentColor: Colors.cyan[600],
      ),
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text(getTittle(_tabController!.index).toString(),
                style: nunitoSansw700sz16),
            bottom: TabBar(
              controller: _tabController,
              onTap: (index) {},
              tabs: myTabs,
              indicatorColor: Color(0xFF28C27D),
              labelColor: Color(0xFF28C27D),
              unselectedLabelColor: Colors.grey,
            ),
          ),
          body: TabBarView(
            controller: _tabController,
            children: [
              RefreshIndicator(
                onRefresh: () async {
                  debugPrint('Refresh');
                },
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(
                            vertical: _sizeScreen.hp(0.5),
                            horizontal: _sizeScreen.wp(1.5)),
                        decoration: BoxDecoration(color: Color(0xFEF6F6F6)),
                        height: _sizeScreen.hp(3),
                        child: Text(
                          '7 Apr 2021',
                          style: nunitoSansw400sz12C12162C,
                        ),
                      ),
                      Container(child: AnnouncementTab()),
                      Divider(),
                      SizedBox(
                        height: _sizeScreen.hp(1),
                      ),
                      Center(
                        child: TextButton(
                          onPressed: () => debugPrint(
                              'Lihat Semua Notifikasi Pemberitahuan'),
                          child: Text(
                            'Lihat Semua',
                            style: nunitoSansw600sz16C0A66C2,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: _sizeScreen.hp(3),
                      padding: EdgeInsets.only(top: _sizeScreen.hp(0.5)),
                      margin: EdgeInsets.only(left: _sizeScreen.wp(1.5)),
                      decoration: BoxDecoration(color: Color(0xFEF6F6F6)),
                      child: Text(
                        '7 Apr 2021',
                        style: nunitoSansw400sz12C12162C,
                      ),
                    ),
                    ChatTab(),
                    SizedBox(
                      height: _sizeScreen.hp(5),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: TextButton(
                        onPressed: () =>
                            debugPrint('Lihat Semua Notifikasi Chat'),
                        child: Text(
                          'Lihat Semua',
                          style: nunitoSansw600sz16C0A66C2,
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
