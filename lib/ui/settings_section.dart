import 'package:flutter/material.dart';
import 'package:popover/popover.dart';
import 'package:provider/provider.dart';
import 'package:survey_app/state_management/idm_notifier.dart';
import 'package:survey_app/utils/custom_widgets/custom_widgets.dart';
import 'package:survey_app/utils/utils.dart';

class SettingTrailing extends StatelessWidget {
  const SettingTrailing({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return InkWell(
      onTap: () => showPopover(
        context: context,
        transitionDuration: const Duration(milliseconds: 150),
        bodyBuilder: (context) => SettingsSection(),
        onPop: () => print('Popover was popped!'),
        direction: PopoverDirection.top,
        // width: _sizeScreen.wp(15),
        // height: _sizeScreen.hp(14),
        width: _sizeScreen.wp(12),
        height: _sizeScreen.hp(21),
        arrowHeight: 15,
        arrowWidth: 30,
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Color(0xFE39225A),
        ),
        padding: EdgeInsets.only(left: _sizeScreen.wp(3)),
        child: Row(
          children: [
            Icon(
              Icons.settings_outlined,
              color: Colors.white,
            ),
            SizedBox(
              width: _sizeScreen.wp(2),
            ),
            Text(
              'Pengaturan',
              style: nunitoSansw700sz14ccwhite,
            )
          ],
        ),
      ),
    );
  }
}

class SettingsSection extends StatelessWidget {
  const SettingsSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
      ),
      padding: EdgeInsets.symmetric(vertical: _sizeScreen.hp(1)),
      child: Column(
        children: [
          Container(
            alignment: Alignment.center,
            height: _sizeScreen.hp(4),
            child: ListTile(
              onTap: () => debugPrint('TODO Clicked on Informasi Akun'),
              dense: true,
              contentPadding:
                  EdgeInsets.symmetric(horizontal: _sizeScreen.wp(1)),
              leading: Container(
                width: 24,
                height: 24,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: Color(0xFFF6F6F6)),
                child: Center(child: Icon(Icons.person_outline)),
              ),
              title: Text('Informasi Akun', style: nunitoSansw400sz14),
            ),
          ),
          SizedBox(
            height: _sizeScreen.hp(1),
          ),
          Divider(
            color: Color(0xFEC7C8CD),
          ),
          Container(
            alignment: Alignment.center,
            height: _sizeScreen.hp(4),
            child: ListTile(
              onTap: () => debugPrint('TODO Clicked on Ubah Password'),
              dense: true,
              contentPadding:
                  EdgeInsets.symmetric(horizontal: _sizeScreen.wp(1)),
              leading: Container(
                width: 24,
                height: 24,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: Color(0xFFF6F6F6)),
                child: Center(
                    child: Image.asset('assets/icons/icon_padlock2.png')),
              ),
              title: Text('Ubah Password', style: nunitoSansw400sz14),
            ),
          ),
          SizedBox(
            height: _sizeScreen.hp(1.5),
          ),
          Divider(
            height: 0.5,
            color: Color(0xFEC7C8CD),
          ),
          Container(
            alignment: Alignment.center,
            height: _sizeScreen.hp(4),
            color: Color(0xFFF6F7FB),
            child: ListTile(
              onTap: () => Provider.of<IDMNotifier>(context, listen: false)
                  .logout(context),
              dense: true,
              contentPadding:
                  EdgeInsets.symmetric(horizontal: _sizeScreen.wp(1)),
              leading: Container(
                width: 24,
                height: 24,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: Color(0xFFF6F6F6)),
                child: Center(
                    child: Image.asset('assets/icons/icon_sign_out.png')),
              ),
              title: Text('Logout', style: nunitoSansw400sz14C4F5262),
            ),
          ),
        ],
      ),
    );
  }
}
