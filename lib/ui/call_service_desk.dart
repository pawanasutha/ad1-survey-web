import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:survey_app/state_management/idm_notifier.dart';
import 'package:survey_app/utils/custom_widgets/custom_widgets.dart';
import 'package:survey_app/utils/utils.dart';

class CallServiceDesk extends StatelessWidget {
  final String helpDeskNo;
  final String helpDeskMail;

  const CallServiceDesk(
      {Key? key, required this.helpDeskNo, required this.helpDeskMail})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: _sizeScreen.hp(1),
        ),
        Center(
          child: Text(
            'Hubungi Service Desk',
            style: nunitoSansw700sz16,
          ),
        ),
        SizedBox(
          height: _sizeScreen.hp(2),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: _sizeScreen.wp(2)),
          height: _sizeScreen.hp(3),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: Colors.white, width: 3),
                  ),
                  child: Icon(Icons.call_outlined)),
              Spacer(),
              Text(
                helpDeskNo,
                style: nunitoSansw400sz14,
                overflow: TextOverflow.ellipsis,
              ),
              Spacer(),
              InkWell(
                child: Icon(Icons.file_copy_outlined),
                onTap: () => Provider.of<IDMNotifier>(context, listen: false)
                    .copyNoServiceDesk(helpDeskNo),
              ),
            ],
          ),
        ),
        SizedBox(
          height: _sizeScreen.hp(1),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: _sizeScreen.wp(2)),
          height: _sizeScreen.hp(3),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.white, width: 3),
                ),
                child: Icon(Icons.mail_outlined),
              ),
              Spacer(),
              Text(
                helpDeskMail,
                style: nunitoSansw400sz14,
                overflow: TextOverflow.ellipsis,
              ),
              Spacer(),
              InkWell(
                child: Icon(Icons.file_copy_outlined),
                onTap: () => Provider.of<IDMNotifier>(context, listen: false)
                    .copyEmailServiceDesk(helpDeskMail),
              ),
            ],
          ),
        )
      ],
    );
  }
}
