import 'package:flutter/material.dart';

class HomeNotifier extends ChangeNotifier {
  bool _isTaskListDataPreview = false;

  /* bool _isTargetDataPreview = true;*/

  List<String> _statusOrders = [
    'All Data',
    "Menunggu Proses Survey",
    "Menunggu Kredit PACIA",
    "Menunggu Resurvey",
    "Menunggu Dakor",
    "Menunggu Conditional Approval",
    "Menunggu Negosiasi"
  ];

  ///Getter / Setter
  /*bool get isTargetDataPreview => _isTargetDataPreview;

  set isTargetDataPreview(bool value) {
    _isTargetDataPreview = value;
    notifyListeners();
  }*/

  bool get isTaskListDataPreview => _isTaskListDataPreview;

  set isTaskListDataPreview(bool value) {
    _isTaskListDataPreview = value;
    notifyListeners();
  }

  List<String> get statusOrders => _statusOrders;

  ///Function

}
