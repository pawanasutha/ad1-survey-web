import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:survey_app/models/tasklist/model_tasklist.dart';
import 'package:survey_app/utils/utils.dart';

class TaskListNotifier extends ChangeNotifier {
  DateTime today = DateTime.now();

  List<ModelTaskList> _data = [];

  List<ModelTaskList> get data => _data;

  ///Logger
  Logger logger = Logger();

  ///GetData

  Future getAllData() async {
    List<ModelTaskList> _allData = [
      ModelTaskList(
          orderNo: "1",
          orderDate: DateTime(2021, 5, 20),
          custName: "Jaya Suparno",
          custType: "Per",
          custAddress:
              "Jl. Jend. Sudirman Kav. 25, RT.10/RW.1, Kuningan, Karet, Kecamatan Setiabudi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12920",
          custPhoneNumber: "+6281282070031",
          lastKnownHandledBy: "MENUNGGU SRE",
          sourceApplication: "outofnowhere",
          priority: "High",
          objectName: "Motor",
          objectType: "Motor Baru",
          objectBrand: "Honda",
          objectModel: "Vario 150",
          latitude: -6.212559,
          longitude: 106.8186947,
          surveyDate: DateTime.now()),
      ModelTaskList(
          orderNo: "2",
          orderDate: DateTime(2021, 5, 20),
          custName: "Sunarnu Jayapri",
          custType: "Per",
          custAddress:
              "Jl. Jambore No.1, RW.7, Cibubur, Kec. Ciracas, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13720",
          custPhoneNumber: "+6285691510622",
          lastKnownHandledBy: "MENUNGGU SRE",
          sourceApplication: "outofnowhere",
          priority: "Medium",
          objectName: "Motor",
          objectType: "Motor Baru",
          objectBrand: "Honda",
          objectModel: "ADV 150",
          latitude: -6.3696117,
          longitude: 106.89203257,
          surveyDate: DateTime.now()),
      ModelTaskList(
          orderNo: "3",
          orderDate: DateTime(2021, 5, 20),
          custName: "Celena Sari",
          custType: "Per",
          custAddress:
              "Kayuringin Jaya, Bekasi Selatan, Bekasi City, West Java",
          custPhoneNumber: "+6287741544882",
          lastKnownHandledBy: "MENUNGGU SRE",
          sourceApplication: "outofnowhere",
          priority: "Low",
          objectName: "Motor",
          objectType: "Motor Baru",
          objectBrand: "Honda",
          objectModel: "Beat Street",
          latitude: -6.2467101,
          longitude: 106.989186,
          surveyDate: DateTime.now()),
      ModelTaskList(
          orderNo: "4",
          orderDate: DateTime(2021, 5, 18),
          custName: "Handoko Budi Budi",
          custType: "per",
          custAddress:
              "Jl. Letjen S. Parman No.kav.28, RT.12/RW.6, Tj. Duren Sel., Kec. Grogol petamburan, Kota Jakarta Barat, Daerah Khusus Ibukota Jakarta 11470",
          custPhoneNumber: "+6287889021069",
          lastKnownHandledBy: "MENUNGGU SRE",
          sourceApplication: "outofnowhere",
          priority: "High",
          objectName: "Motor",
          objectType: "Motor Baru",
          objectBrand: "Honda",
          objectModel: "CBR 150",
          latitude: -6.1773633,
          longitude: 106.7884836,
          surveyDate: DateTime.now()),
      ModelTaskList(
          orderNo: "5",
          orderDate: DateTime(2021, 5, 18),
          custName: "Sudirman Sudjjjjjjjaaaaaang",
          custType: "Per",
          custPhoneNumber: "",
          custAddress: "Coordinate 0,0",
          lastKnownHandledBy: "MENUNGGU SRE",
          sourceApplication: "outofnowhere",
          priority: "High",
          objectName: "Motor",
          objectType: "Motor Baru",
          objectBrand: "Honda",
          objectModel: "Supra GTR",
          latitude: 1,
          surveyDate: DateTime.now()),
      ModelTaskList(
          orderNo: "6",
          orderDate: DateTime(2021, 5, 18),
          custName: "Alya Andaman",
          custType: "Per",
          custAddress: "Coordinate 0,1",
          custPhoneNumber: "+62123123123",
          lastKnownHandledBy: "MENUNGGU PAC-IA",
          sourceApplication: "outofnowhere",
          priority: "Medium",
          objectName: "Motor",
          objectType: "Motor Baru",
          objectBrand: "Honda",
          objectModel: "New Revo",
          longitude: 1,
          surveyDate: DateTime.now()),
    ];
    _data = _allData;
    notifyListeners();
  }

  int countOrderByDate(String date) {
    var result = _data
        .where((element) =>
            DateFormat('dd-MMMM-yyyy').format(element.orderDate!) == date)
        .toList()
        .length;
    logger.i(
        date,
        _data
            .where((element) =>
                DateFormat('dd-MMMM-yyyy').format(element.orderDate!) == date)
            .toList()
            .length);
    return result;
  }
}
