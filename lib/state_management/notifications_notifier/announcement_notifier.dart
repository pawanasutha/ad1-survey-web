part of 'notifications_notifier.dart';

class AnnouncementNotifier extends ChangeNotifier {
  List<ModelAnnouncement> modelNotifications = [];

  Logger logger = Logger();

  Future getDataNotifications() async {
    logger.i('Get Data List Announcement');
    return modelNotifications = [
      ModelAnnouncement(
        isRead: true,
        title: 'Astri • Vario 150',
        message: 'Order diterima. Lakukan survey sesuai jadwal ...',
        timeReceive: '16.30',
        status: 'MENUNGGU SRE',
      ),
      ModelAnnouncement(
        isRead: false,
        title: 'Sunarnu • ADV 150',
        message: 'Order diterima. Lakukan survey sesuai jadwal ...',
        timeReceive: '16.30',
        status: 'MENUNGGU SRE',
      ),
      ModelAnnouncement(
        isRead: false,
        title: 'Celena • Beat Street',
        message: 'Order diterima. Lakukan survey sesuai jadwal ...',
        timeReceive: '16.30',
        status: 'MENUNGGU SRE',
      ),
    ];
  }

  onRefresh(mounted) {
    modelNotifications.clear();
    getDataNotifications();
    notifyListeners();
  }
}
