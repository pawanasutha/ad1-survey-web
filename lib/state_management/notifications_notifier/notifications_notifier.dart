import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:survey_app/models/model_notifications/model_announcement.dart';
import 'package:survey_app/models/model_notifications/model_chat.dart';

part 'announcement_notifier.dart';
part 'chat_notifier.dart';
