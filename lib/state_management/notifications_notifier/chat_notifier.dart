part of 'notifications_notifier.dart';

class ChatNotifier extends ChangeNotifier {
  List<ModelChat> modelChat = [];

  Logger logger = Logger();

  Future getDataChat() async {
    logger.i('Get Data List Chat');
    return modelChat = [
      ModelChat(
        isRead: true,
        title: 'Andre • Sales Officer',
        message: 'Wah kok jadi ribet gitu yak wkwk. Oke biar ku bantu ... ',
        timeReceive: '09.00',
      ),
      ModelChat(
        isRead: false,
        title: 'Andre • Sales Officer',
        message: 'Wah kok jadi ribet gitu yak wkwk. Oke biar ku bantu ... ',
        timeReceive: '09.00',
      ),
      ModelChat(
        isRead: true,
        title: 'Andre • Sales Officer',
        message: 'Wah kok jadi ribet gitu yak wkwk. Oke biar ku bantu ... ',
        timeReceive: '09.00',
      ),
    ];
  }

  onRefresh(mounted) {
    modelChat.clear();
    getDataChat();
    notifyListeners();
  }
}
