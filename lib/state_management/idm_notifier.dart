import 'dart:async';
import 'dart:core';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:logger/logger.dart';
import 'package:popover/popover.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:survey_app/models/model_user.dart';
import 'package:survey_app/services/services.dart';
import 'package:survey_app/state_management/tasklist_notifier.dart';
import 'package:survey_app/ui/call_service_desk.dart';
import 'package:survey_app/utils/custom_widgets/custom_widgets.dart';
import 'package:survey_app/utils/utils.dart';

import 'multipurpose_notifier.dart';
import 'notifications_notifier/notifications_notifier.dart';

class IDMNotifier with ChangeNotifier {
  GlobalKey<FormState> _globalLoginKey = new GlobalKey<FormState>();
  GlobalKey<FormState> _globalForgotPasswordKey = new GlobalKey<FormState>();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();

  ///Lupa Password
  TextEditingController _controllerEmailRegistered = TextEditingController();
  static final DateTime now = DateTime.now();
  bool _secureText = true;
  bool _autoValidate = false;
  bool _loginProcess = false;
  bool _widgetEnable = true;
  bool _rememberMe = false;
  String _helpDeskNo = '021-5489556';
  String _helpDeskEmail = 'service.desk@adira.co.id';

  ///Service
  Ad1AccessService ad1Access = Ad1AccessService();

  ///Logger
  var logger = Logger();

  ///Todo UserLogin From API
  /*UserLoginDatabase _userLoginDatabase = UserLoginDatabase();*/
  ModelUser user = new ModelUser();

  ///Getter & Setter
  bool get rememberMe => _rememberMe;

  set rememberMe(bool value) {
    _rememberMe = value;
    notifyListeners();
  }

  GlobalKey<FormState> get globalLoginKey => _globalLoginKey;

  GlobalKey<FormState> get globalForgotPasswordKey => _globalForgotPasswordKey;

  TextEditingController get controllerEmail => _controllerEmail;

  TextEditingController get controllerPassword => _controllerPassword;

  TextEditingController get controllerEmailRegistered =>
      _controllerEmailRegistered;

  bool get secureText => _secureText;

  set secureText(bool value) {
    this._secureText = value;
    notifyListeners();
  }

  bool get loginProcess => _loginProcess;

  set loginProcess(bool value) {
    _loginProcess = value;
    notifyListeners();
  }

  bool get widgetEnable => _widgetEnable;

  set widgetEnable(bool value) {
    _widgetEnable = value;
    notifyListeners();
  }

  bool get autoValidate => _autoValidate;

  set autoValidate(bool value) {
    this._autoValidate = value;
    notifyListeners();
  }

  ///Function

  void showHidePass() {
    _secureText = !_secureText;
    notifyListeners();
  }

  authenticationUser(BuildContext context) async {
    if (_globalLoginKey.currentState!.validate() != false) {
      loginProcess = true;
      _widgetEnable = false;

      SharedPreferences _preferences = await SharedPreferences.getInstance();
      try {
        var result = await ad1Access.authenticateUser(
            _controllerEmail.text, _controllerPassword.text);
        _preferences.setString("NIK", _controllerEmail.text);
        print("Saved Preference on NIK " + _controllerEmail.text);

        ///ForTestingPurpose
        //  var result ="OK";
        if (result == "OK") {
          fetchData(context);
          logger.i('Success Login');
          Get.offNamed(AppLevelConstants.dashboardRoute);
          _preferences.setString("statusLogin", "alreadyLogin");
          if (rememberMe == true) {
            _preferences.setString("flagRememberMe", rememberMe.toString());
            logger.i("savedPreference Remember Me " + rememberMe.toString());

            ///FIXME While Insert Into SQLite
            /*saveUserLoginSqlite();*/
          } else if (rememberMe == false) {
            _preferences.setString("flagRememberMe", rememberMe.toString());
            logger.i("savedPreference Remember Me " + rememberMe.toString());
          }
        } else if (result == "ExpiredPassword") {
          showWarningDialog("Your Password Expired", context);
        } else if (result == "LockedAccount") {
          showWarningDialog("Your Account Was Locked", context);
        } else if (result == "WrongPassword") {
          showWarningDialog("Wrong Password", context);
        } else {
          CustomShowSnackBar.basicRedSnackBar(context, result.toString());
        }
      } catch (e) {
        debugPrint("catch on provider auth " + e.toString());
        CustomShowSnackBar.basicRedSnackBar(
            context, "Catch on Provider Auth " + e.toString());
      }
      loginProcess = false;
      _widgetEnable = true;
    } else {
      autoValidate = true;
    }
  }

  forgetUser(BuildContext context) async {
    if (_globalForgotPasswordKey.currentState!.validate() != false) {
      ///TODO Create Function to Send to API
      debugPrint(
          'Forgot Password With Email ${_controllerEmailRegistered.text}');
      Navigator.pushNamed(context, AppLevelConstants.verificationRoute);
    } else {
      autoValidate = true;
    }
  }

  Future<ModelUser?> getNameSurveyor(BuildContext context) async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    String nik = _preference.getString("NIK")!;
    try {
      Ad1AccessService authService = new Ad1AccessService();
      await authService.getUserProfile(nik).then((value) {
        user = value;
      });
      print('Value Get User is $user');
      return user;
    } catch (e) {
      showSnackBar(context, e.toString());
    }
    return user;
  }

  Future<void> copyNoServiceDesk(String serviceDeskNo) {
    return Clipboard.setData(ClipboardData(text: serviceDeskNo));
  }

  Future<void> copyEmailServiceDesk(String emailServiceDesk) {
    return Clipboard.setData(ClipboardData(text: emailServiceDesk));
  }

  Future<dynamic> showPopupHelp(BuildContext context) {
    return showPopover(
      context: context,
      transitionDuration: const Duration(milliseconds: 150),
      bodyBuilder: (context) => CallServiceDesk(
          helpDeskNo: _helpDeskNo, helpDeskMail: _helpDeskEmail),
      onPop: () => print('Popover was popped!'),
      direction: PopoverDirection.top,
      // width: _sizeScreen.wp(15),
      // height: _sizeScreen.hp(14),
      width: 290,
      height: 135,
      arrowHeight: 15,
      arrowWidth: 30,
    );
  }

  void logout(BuildContext context) async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    var flagRememberMe = _preference.getString("flagRememberMe");
    logger.i("Value FlagRememberMe $flagRememberMe");
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return CustomPopupAlert(
          contentTittle: 'Keluar Dari Aplikasi',
          contentSubTittle: 'Apakah Anda yakin untuk keluar dari aplikasi ?',
          contentButton1: 'Tidak',
          contentButton2: 'Ya',
          functionsButton1: () => Navigator.of(context).pop(),
          functionsButton2: () => checkRememberMe(flagRememberMe, context),
        );
      },
    );
  }

  void checkRememberMe(flagRememberMe, context) {
    if (flagRememberMe == "false") {
      clearProviderLogin(context);
      clearSharedPreference(context);
      clearData(context);
      print("Clear Provider and stuff");
      Get.offNamed(AppLevelConstants.homeRoute);
    } else {
      clearSharedPreference(context);
      rememberMe = false;
      autoValidate = false;
      widgetEnable = true;
      Get.offAndToNamed(AppLevelConstants.homeRoute);
    }
  }

  void clearSharedPreference(BuildContext context) async {
    SharedPreferences _preference = await SharedPreferences.getInstance();
    _preference.clear();
  }

  void clearProviderLogin(BuildContext context) async {
    controllerEmail.clear();
    controllerPassword.clear();
    autoValidate = false;
    widgetEnable = true;
  }

  void fetchData(BuildContext context) {
    ///Get Data Tasklist
    Future.delayed(Duration(milliseconds: 0)).then((_) {
      context.read<TaskListNotifier>().getAllData();
    });

    ///Get Data Notification (Chat / Announcement Tasklist )
    Future.delayed(Duration(milliseconds: 0)).then((_) {
      context.read<AnnouncementNotifier>().getDataNotifications();
    });

    Future.delayed(Duration(milliseconds: 0)).then((_) {
      context.read<ChatNotifier>().getDataChat();
    });

    ///Get List Month For 1 Year Above
    Future.delayed(Duration(milliseconds: 0)).then((_) {
      context.read<MultipurposeNotifier>().getListOfMonthAYear();
    });
  }

  void clearData(BuildContext context) async {
    ///TODO For Clear All Data Inside Application After Logout
  }

  showSnackBar(BuildContext context, String text) {
    print("value for snackbar @Home $text");
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("$text"),
        behavior: SnackBarBehavior.floating,
        backgroundColor: Color(0xfff0382b),
        duration: Duration(seconds: 2)));
  }

/*void saveUserLoginSqlite() async {
    print("save to SQLite" +
        _controllerEmail.text +
        _controllerPassword.text +
        now.toString());
    _userLoginDatabase.insertUserLogin(
        UserLogin(_controllerEmail.text, _controllerPassword.text));
    List _userLogin = await _userLoginDatabase.getUserLogin();
    print(_userLogin);
  }*/

}
