part of 'services.dart';

class Ad1AccessService {
  Future authenticateUser(String id, String password) async {
    try {
      final resAuth = await http.post(
        Uri.parse("${Config.baseUrlAuth}api/AuthenticateUser"),
        body: {"login": id, "password": password},
      ).timeout(Duration(seconds: 10));
      if (resAuth.statusCode == 200) {
        return resAuth.body;
      }
    } on TimeoutException catch (_) {
      throw Exception(Config.timeoutConnection);
    } catch (e) {
      throw Exception(e.toString());
    }
    return false;
  }

  Future changePassword(
      String id, String oldPassword, String newPassword) async {
    try {
      final resAuth = await http.post(
        Uri.parse("${Config.baseUrlAuth}api/ChangePassword"),
        body: {
          "login": id,
          "password": oldPassword,
          "newpassword": newPassword
        },
      ).timeout(Duration(seconds: 10));
      if (resAuth.statusCode == 200) {
        print("ChangePassword " +
            "id $id" +
            " " +
            "oldpassword $oldPassword" +
            " " +
            "newpassword $newPassword" +
            " the result is " +
            resAuth.body);
        return resAuth.body;
      }
    } on TimeoutException catch (_) {
      throw Exception(Config.timeoutConnection);
    } catch (e) {
      throw Exception(e.toString());
    }
    return false;
  }

  Future getUserProperty(String id) async {
    try {
      final resAuth = await http.post(
        Uri.parse("${Config.baseUrlAuth}api/getUserProperty"),
        body: {"login": id},
      ).timeout(Duration(seconds: 10));
      if (resAuth.statusCode == 200) {
        return resAuth.body;
      }
    } on TimeoutException catch (_) {
      throw Exception(Config.timeoutConnection);
    } catch (e) {
      throw Exception(e.toString());
    }
    return false;
  }

  Future getUserPropertyCurentOUID(String id) async {
    try {
      final resAuth = await http.post(
        Uri.parse("${Config.baseUrlAuth}api/GetUserPropertyCurentOUID"),
        body: {"login": id},
      ).timeout(Duration(seconds: 10));
      if (resAuth.statusCode == 200) {
        return resAuth.body;
      }
    } on TimeoutException catch (_) {
      throw Exception(Config.timeoutConnection);
    } catch (e) {
      throw Exception(e.toString());
    }
    return false;
  }

  Future getUserPropertyWithRealOU(String id) async {
    try {
      final resAuth = await http.post(
        Uri.parse("${Config.baseUrlAuth}api/GetUserPropertyWithRealOU"),
        body: {"login": id},
      ).timeout(Duration(seconds: 10));
      if (resAuth.statusCode == 200) {
        return resAuth.body;
      }
    } on TimeoutException catch (_) {
      throw Exception(Config.timeoutConnection);
    } catch (e) {
      return (e.toString());
    }
    return false;
  }

  Future<ModelUser> getUserProfile(String id) async {
    try {
      final resAuth = await http.post(
        Uri.parse("${Config.baseUrlAuth}api/GetUserPropertyWithRealOU"),
        body: {"login": id},
      ).timeout(Duration(seconds: 10));
      if (resAuth.statusCode == 200) {
        return ModelUser.fromJson(jsonDecode(resAuth.body));
      }
      return throw ArgumentError('Failed Access API');
    } on TimeoutException catch (_) {
      throw Exception(Config.timeoutConnection);
    } catch (e) {
      throw ArgumentError('$e.toString');
    }
  }

  Future resetPassword(String nik, String phoneNo, bool isSms) async {
    try {
      final resAuth = await http.post(
        Uri.parse("${Config.baseUrlAuth}api/ResetPassword"),
        body: {"login": nik, "noHp": phoneNo, "isSms": isSms.toString()},
      ).timeout(Duration(seconds: 10));
      if (resAuth.statusCode == 200) {
        print("Reset Password " +
            "id $nik" +
            " " +
            "Mobile Number $phoneNo" +
            " " +
            "Flag SMS $isSms" +
            " the result is " +
            resAuth.body);
        return resAuth.body;
      }
    } on TimeoutException catch (_) {
      throw Exception(Config.timeoutConnection);
    } catch (e) {
      throw Exception(e.toString());
    }
    return false;
  }
}
