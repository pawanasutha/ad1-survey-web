class ModelOrganisasiUnit {
  String? parentObjectAlias;
  String? parentObjectName;
  String? propertyValue;
  String? ouType;

  ModelOrganisasiUnit(
      {this.parentObjectAlias,
      this.parentObjectName,
      this.propertyValue,
      this.ouType});

  factory ModelOrganisasiUnit.fromJson(Map<String, dynamic> json) {
    return ModelOrganisasiUnit(
        parentObjectAlias: json['ParentObjectAlias'],
        parentObjectName: json['ParentObjectName'],
        propertyValue: json['PropertyValue'],
        ouType: json['OUtype']);
  }

  @override
  String toString() {
    return 'OrganisasiUnit{${this.parentObjectAlias}, ${this.parentObjectName},${this.propertyValue},${this.ouType} }';
  }
}
