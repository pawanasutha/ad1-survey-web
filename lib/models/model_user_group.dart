class ModelGroup {
  String? parentObjectAlias;
  String? parentObjectName;
  String? jobGroup;

  ModelGroup({this.parentObjectAlias, this.parentObjectName, this.jobGroup});

  factory ModelGroup.fromJson(Map<String, dynamic> json) {
    return ModelGroup(
        parentObjectAlias: json['ParentObjectAlias'],
        parentObjectName: json['ParentObjectName'],
        jobGroup: json['JobGroup']);
  }

  @override
  String toString() {
    return 'Group{${this.parentObjectAlias}, ${this.parentObjectName},${this.jobGroup} }';
  }
}
