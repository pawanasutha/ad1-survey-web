class ModelTaskList {
  final String? orderNo;
  final DateTime? orderDate;
  final String? custName;
  final String? custType;
  final String? custAddress;
  final String? custPhoneNumber;
  final String? lastKnownHandledBy;
  final String? sourceApplication;
  final String? priority;
  final String? objectName;
  final String? objectType;
  final String? objectBrand;
  final String? objectModel;
  final DateTime? surveyDate;
  final double latitude;
  final double longitude;
  final String? noteCa;
  final List<String>? documentResurvey;

  ModelTaskList(
      {this.orderNo,
      this.orderDate,
      this.custName,
      this.custType,
      this.custAddress,
      this.custPhoneNumber,
      this.lastKnownHandledBy,
      this.sourceApplication,
      this.priority,
      this.objectName,
      this.objectType,
      this.objectBrand,
      this.objectModel,
      this.surveyDate,
      this.latitude = 0,
      this.longitude = 0,
      this.noteCa,
      this.documentResurvey});

  @override
  String toString() {
    return '{ ${this.orderNo},${this.orderDate} ${this.custName} ${this.custType} ${this.custAddress} '
        '${this.custPhoneNumber} ${this.lastKnownHandledBy} ${this.sourceApplication} ${this.priority} ${this.objectName} ${this.latitude},${this.lastKnownHandledBy} ,${this.longitude}, ${this.sourceApplication} }';
  }
}
