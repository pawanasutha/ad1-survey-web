class ModelAnnouncement {
  final bool isRead;
  final String? title;
  final String? message;
  final String? timeReceive;
  final String? status;

  ModelAnnouncement(
      {required this.isRead,
      this.title,
      this.message,
      this.timeReceive,
      this.status});
}
