class ModelChat {
  final bool isRead;
  final String? title;
  final String? message;
  final String? timeReceive;

  ModelChat({
    required this.isRead,
    this.title,
    this.message,
    this.timeReceive,
  });
}
