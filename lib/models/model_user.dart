import 'model_user_group.dart';
import 'model_user_organisasi_unit.dart';

class ModelUser {
  String? objectName;
  String? login;
  String? email;
  String? noHp;
  String? namaRekening;
  String? noRekening;
  String? orgDepartment;
  String? orgPosition;
  List<ModelGroup>? groups;
  List<ModelOrganisasiUnit>? organisasiUnits;

  ModelUser(
      {this.objectName,
      this.login,
      this.email,
      this.noHp,
      this.namaRekening,
      this.noRekening,
      this.orgDepartment,
      this.orgPosition,
      this.groups,
      this.organisasiUnits});

  factory ModelUser.fromJson(Map<String, dynamic> json) {
    return ModelUser(
        objectName: json["objectName"],
        login: json["login"],
        email: json["email"],
        noHp: json["noHp"],
        namaRekening: json["namaRekening"],
        noRekening: json["noRekening"],
        orgDepartment: json["orgDepartment"],
        orgPosition: json["orgPosition"],
        groups: List<ModelGroup>.from(json["group"].map((groups) {
          return ModelGroup.fromJson(groups);
        })),
        organisasiUnits: List<ModelOrganisasiUnit>.from(
            json["organisasiUnit"].map((organisasiUnits) {
          return ModelOrganisasiUnit.fromJson(organisasiUnits);
        })));
  }

  @override
  String toString() {
    return '{ ${this.objectName}, ${this.login}, ${this.email}, ${this.groups} }';
  }
}
