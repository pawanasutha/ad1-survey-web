import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:provider/provider.dart';
import 'package:survey_app/state_management/home_notifier.dart';
import 'package:survey_app/state_management/idm_notifier.dart';
import 'package:survey_app/state_management/multipurpose_notifier.dart';
import 'package:survey_app/state_management/notifications_notifier/notifications_notifier.dart';
import 'package:survey_app/state_management/tasklist_notifier.dart';
import 'package:survey_app/ui/dashboard/main_task/ide_pages/main_task_ide_page.dart';
import 'package:survey_app/ui/undefined_page.dart';
import 'package:survey_app/utils/custom_widgets/custom_widgets.dart';
import 'package:survey_app/utils/route/observer/custom_route_observer.dart';
import 'package:survey_app/utils/utils.dart';

void main() {
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (context) => Validation()),
      ChangeNotifierProvider(create: (context) => IDMNotifier()),
      ChangeNotifierProvider(create: (context) => HomeNotifier()),
      ChangeNotifierProvider(create: (context) => TaskListNotifier()),
      ChangeNotifierProvider(create: (context) => AnnouncementNotifier()),
      ChangeNotifierProvider(create: (context) => ChatNotifier()),
      ChangeNotifierProvider(create: (context) => MultipurposeNotifier()),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: AppLevelConstants.titleOnTab,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      //home: MainTaskIDEPage(),
      initialRoute: AppLevelConstants.homeRoute,
      onUnknownRoute: (settings) => MaterialPageRoute<dynamic>(
        builder: (context) => UndefinedView(name: settings.name),
      ),
      onGenerateRoute: CustomRoute.generateRoute,

      ///FIXME Here to Get Route History
      //navigatorObservers: [CustomRouteObserver()],
    );
  }
}
