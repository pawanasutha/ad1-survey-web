part of 'utils.dart';

const listOfMonth = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

String formatDateddMMyyyy(DateTime initialDate) {
  DateFormat _formatDateddMMyyyy = DateFormat("dd-MM-yyyy");
  return _formatDateddMMyyyy.format(initialDate);
}

String formatDateddMMMyyyy(DateTime initialDate) {
  DateFormat _formatDateddMMyyyy = DateFormat("dd-MMM-yyyy");
  return _formatDateddMMyyyy.format(initialDate);
}

String formatDateMMM(DateTime initialDate) {
  DateFormat _formatDateTimeMMM = DateFormat("MMM");
  return _formatDateTimeMMM.format(initialDate);
}

String formatDateMMMYYYY(DateTime initialDate) {
  DateFormat _formatDateTimeMMM = DateFormat("MMM-yyyy");
  return _formatDateTimeMMM.format(initialDate);
}

String formatTimeHHmmss(DateTime initialDate) {
  DateFormat _formatDateTimeHHmmss = DateFormat("HH:mm:ss");
  return _formatDateTimeHHmmss.format(initialDate);
}

String formatTimeHHmm(DateTime initialDate) {
  DateFormat _formatDateTimeHHmmss = DateFormat("HH:mm");
  return _formatDateTimeHHmmss.format(initialDate);
}

formatCurrency(String value) {
  double unFormatted = double.parse(value);
  var _formatCurrency = new NumberFormat.currency(locale: "ID", symbol: "");
  return _formatCurrency.format(unFormatted);
}
