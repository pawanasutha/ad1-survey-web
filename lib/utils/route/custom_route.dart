part of '../utils.dart';

class CustomRoute {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case AppLevelConstants.homeRoute:
        return _NoAnimationMaterialPageRoute<dynamic>(
          builder: (_) => const LoginPage(),
          name: AppLevelConstants.homeRoute,
        );

      case AppLevelConstants.forgotPasswordRoute:
        return _NoAnimationMaterialPageRoute<dynamic>(
          builder: (_) => const ForgotPasswordPage(),
          name: AppLevelConstants.forgotPasswordRoute,
        );

      case AppLevelConstants.verificationOTPRoute:
        return _NoAnimationMaterialPageRoute<dynamic>(
          builder: (_) => const VerificationOTPPage(),
          name: AppLevelConstants.verificationOTPRoute,
        );

      case AppLevelConstants.verificationRoute:
        return _NoAnimationMaterialPageRoute<dynamic>(
          builder: (_) => const VerificationPage(),
          name: AppLevelConstants.verificationRoute,
        );

      case AppLevelConstants.dashboardRoute:
        return _NoAnimationMaterialPageRoute<dynamic>(
          builder: (_) => const DashboardPage(),
          name: AppLevelConstants.dashboardRoute,
        );

      default:
        return _NoAnimationMaterialPageRoute<dynamic>(
          builder: (_) => const LoginPage(),
          name: AppLevelConstants.homeRoute,
        );
    }
  }
}

class _NoAnimationMaterialPageRoute<T> extends MaterialPageRoute<T> {
  _NoAnimationMaterialPageRoute({
    required WidgetBuilder builder,
    required String name,
    bool maintainState = true,
    bool fullscreenDialog = false,
  }) : super(
          builder: builder,
          maintainState: maintainState,
          settings: RouteSettings(name: name),
          fullscreenDialog: fullscreenDialog,
        );

  @override
  Widget buildTransitions(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) =>
      child;
}
