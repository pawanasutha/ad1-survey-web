part of '../utils.dart';

class NavigatorUtils {
  static void nextScreen(context, page) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => page));
  }

  static void nextScreeniOS(context, page) {
    Navigator.push(context, CupertinoPageRoute(builder: (context) => page));
  }

  static void nextScreenCloseOthers(context, page) {
    Navigator.pushAndRemoveUntil(context,
        MaterialPageRoute(builder: (context) => page), (route) => false);
  }

  static void nextScreenReplace(context, page) {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => page));
  }

  static Widget notYetFilledText() {
    return Text(
      'Belum Diisi',
      style: GoogleFonts.nunitoSans(
          color: Colors.red,
          fontSize: 14,
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.w400),
    );
  }

  static Iterable<int> range(int min, int max) sync* {
    for (int i = min; i < max; ++i) {
      yield i;
    }
  }

  static String prettyPrint(jsonObject) {
    var encoder = JsonEncoder.withIndent('    ');
    return encoder.convert(jsonObject);
  }
}
