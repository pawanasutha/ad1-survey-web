import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:survey_app/ui/dashboard/dashboard.dart';
import 'package:survey_app/ui/identity_management/forgot_password_page.dart';
import 'package:survey_app/ui/identity_management/login_page.dart';
import 'package:survey_app/ui/identity_management/verification_otp_page.dart';
import 'package:survey_app/ui/identity_management/verification_page.dart';

part 'theme/styling_text.dart';
part 'theme/constant_color.dart';
part 'validation.dart';
part 'constant_value.dart';
part 'config.dart';
part 'app_level_constant.dart';
part 'route/custom_route.dart';
part 'route/navigator_utils.dart';
