part of '../utils.dart';

TextStyle poppinsw600sz24 = GoogleFonts.poppins().copyWith(
    color: Colors.black,
    fontSize: 24,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal);
TextStyle poppinsw600sz22 = GoogleFonts.poppins().copyWith(
    color: Colors.black,
    fontSize: 22,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal);
TextStyle poppinsw600sz20 = GoogleFonts.poppins().copyWith(
    color: Colors.black,
    fontSize: 20,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal);
TextStyle poppinsw600sz20C12162C = GoogleFonts.poppins().copyWith(
    color: Color(0xff12162C),
    fontSize: 20,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal);
TextStyle poppinsw600sz16 = GoogleFonts.poppins().copyWith(
    color: Colors.black,
    fontSize: 16,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal);
TextStyle poppinsw400sz20C1216C = GoogleFonts.poppins().copyWith(
    color: Color(0xFE12162C),
    fontSize: 18,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal);
TextStyle poppinsw600sz18 = GoogleFonts.poppins().copyWith(
    color: Colors.black,
    fontSize: 18,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal);
TextStyle poppinsw600sz18C4F5262 = GoogleFonts.poppins().copyWith(
    color: Color(0xFE4F5262),
    fontSize: 18,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal);
TextStyle poppinsw600sz16ccWhite = GoogleFonts.poppins().copyWith(
    color: Colors.white,
    fontSize: 16,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw700sz16 = GoogleFonts.nunitoSans().copyWith(
    color: Colors.black,
    fontSize: 16,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw700sz16C92949D = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFF92949D),
    fontSize: 16,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw700sz16C28C27D = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFF28C27D),
    fontSize: 16,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw700sz16ccWhite = GoogleFonts.nunitoSans().copyWith(
    color: Colors.white,
    fontSize: 16,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw700sz20 = GoogleFonts.nunitoSans().copyWith(
    color: Colors.black,
    fontSize: 20,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw700sz14ccwhite = GoogleFonts.nunitoSans().copyWith(
    color: Colors.white,
    fontSize: 14,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw700sz14 = GoogleFonts.nunitoSans().copyWith(
    color: Colors.black,
    fontSize: 14,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw700sz14C4F5262 = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFE4F5262),
    fontSize: 14,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw700sz12 = GoogleFonts.nunitoSans().copyWith(
    color: Colors.black,
    fontSize: 12,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw700sz12C92949D = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xff92949D),
    fontSize: 12,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw700sz12ccWhite = GoogleFonts.nunitoSans().copyWith(
    color: Colors.white,
    fontSize: 14,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw600sz16 = GoogleFonts.nunitoSans().copyWith(
    color: Colors.black,
    fontSize: 16,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw600sz16C0A66C2 = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFF0A66C2),
    fontSize: 16,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw600sz12 = GoogleFonts.nunitoSans().copyWith(
    color: Colors.black,
    fontSize: 12,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw600sz14C0A66C2 = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFE0A66C2),
    fontSize: 14,
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz14C4F5262 = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFE4F5262),
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz14C28C27D = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFF28C27D),
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz14C12162C = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFF12162C),
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz14 = GoogleFonts.nunitoSans().copyWith(
    color: Colors.black,
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz18 = GoogleFonts.nunitoSans().copyWith(
    color: Colors.black,
    fontSize: 18,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz20 = GoogleFonts.nunitoSans().copyWith(
    color: Colors.black,
    fontSize: 20,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz20CWhite = GoogleFonts.nunitoSans().copyWith(
    color: Colors.white,
    fontSize: 20,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz16C92949D = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFF92949D),
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz16 = GoogleFonts.nunitoSans().copyWith(
    color: Colors.black,
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz14C92949D = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFE92949D),
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz14C0A66C2 = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFE0A66C2),
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz12 = GoogleFonts.nunitoSans().copyWith(
    color: Colors.black,
    fontSize: 12,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz12C12162C = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFE12162C),
    fontSize: 12,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz12C12162CLt1 = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFE12162C),
    fontSize: 12,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    letterSpacing: 1);
TextStyle nunitoSansw400sz12C92949D = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFE92949D),
    fontSize: 12,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz12C4F5262 = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFE4F5262),
    fontSize: 12,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSans92949Dw400sz14 = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFE92949D),
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz10C92949D = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFE92949D),
    fontSize: 10,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal);
TextStyle nunitoSansw400sz10C3D86CFLt010 = GoogleFonts.nunitoSans().copyWith(
    color: Color(0xFE3D86CF),
    fontSize: 10,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    letterSpacing: 0.10);
