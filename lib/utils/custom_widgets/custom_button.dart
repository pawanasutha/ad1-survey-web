part of 'custom_widgets.dart';

class CustomButton extends StatelessWidget {
  final String textOnButton;
  final Color borderSideColor;
  final TextStyle textStyle;
  final Color backgroundButtonColor;
  final double width;
  final Function() function;

  CustomButton(
      {Key? key,
      required this.textOnButton,
      this.borderSideColor = Colors.lightBlue,
      required this.textStyle,
      required this.backgroundButtonColor,
      this.width = double.infinity,
      required this.function})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      height: _sizeScreen.hp(5),
      width: width,
      child: OutlinedButton(
        style: ButtonStyle(
          side: MaterialStateProperty.all<BorderSide>(
              BorderSide(color: borderSideColor)),
          backgroundColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) {
            if (states.contains(MaterialState.pressed))
              return Colors.lightBlueAccent;
            return backgroundButtonColor;
          }),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(22.0),
            ),
          ),
        ),
        onPressed: function,
        child: Text(
          textOnButton,
          style: textStyle,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
