part of 'custom_widgets.dart';

class CustomChip extends StatelessWidget {
  final String contentText;

  const CustomChip({Key? key, required this.contentText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      height: _sizeScreen.hp(3.5),
      width: _sizeScreen.wp(8),
      decoration: BoxDecoration(
          color: Color(0xFFF6F9FE), borderRadius: BorderRadius.circular(30)),
      child: Center(
        child: Text(
          contentText,
          style: nunitoSansw400sz10C3D86CFLt010,
        ),
      ),
    );
  }
}
