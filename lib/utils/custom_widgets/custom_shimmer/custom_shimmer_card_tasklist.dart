part of 'custom_shimmer.dart';

class CustomShimmerCardTasklist extends StatelessWidget {
  const CustomShimmerCardTasklist({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Container(
      height: _sizeScreen.hp(7),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6.0),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: _sizeScreen.hp(4),
            width: _sizeScreen.wp(3),
            decoration: BoxDecoration(
              color: Colors.grey,
              shape: BoxShape.circle,
            ),
          ),
          SizedBox(
            width: _sizeScreen.wp(0.25),
          ),
          Container(
            width: _sizeScreen.wp(6),
            color: Colors.white,
          ),
          SizedBox(
            width: _sizeScreen.wp(1),
          ),
          Container(
            width: _sizeScreen.wp(6),
            color: Colors.white,
          ),
          SizedBox(
            width: _sizeScreen.wp(1),
          ),
          Container(
            width: _sizeScreen.wp(7),
            color: Colors.white,
          ),
          SizedBox(
            width: _sizeScreen.wp(1),
          ),
          Container(
            width: _sizeScreen.wp(7),
            color: Colors.white,
          ),
          SizedBox(
            width: _sizeScreen.wp(2),
          ),
          Container(
            height: _sizeScreen.hp(3.5),
            width: _sizeScreen.wp(8),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(30)),
          )
        ],
      ),
    );
  }
}
