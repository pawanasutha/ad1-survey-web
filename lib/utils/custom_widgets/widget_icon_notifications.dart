part of 'custom_widgets.dart';

class WidgetIconNotifications extends StatelessWidget {
  final int? totalNotif;

  WidgetIconNotifications({this.totalNotif});

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return GestureDetector(
      onTap: () => showPopover(
        context: context,
        transitionDuration: const Duration(milliseconds: 150),
        bodyBuilder: (context) => NotificationsPage(),
        onPop: () => print('Popover was popped!'),
        direction: PopoverDirection.right,
        // width: _sizeScreen.wp(15),
        // height: _sizeScreen.hp(14),
        width: _sizeScreen.wp(27),
        height: _sizeScreen.hp(62),
        arrowHeight: 15,
        arrowWidth: 30,
      ),
      child: Container(
        height: 24,
        width: 24,
        child: Stack(
          children: [
            Icon(
              Icons.notifications,
              color: Colors.black,
              size: 30,
            ),
            (totalNotif != null)
                ? Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      height: 15,
                      width: 15,
                      decoration: BoxDecoration(
                        color: Color(0xFFF74F2C),
                        shape: BoxShape.circle,
                      ),
                      child: Center(
                        child: Text(totalNotif.toString(),
                            style: GoogleFonts.nunitoSans(
                                fontSize: 12,
                                color: Colors.white,
                                fontWeight: FontWeight.w400)),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
