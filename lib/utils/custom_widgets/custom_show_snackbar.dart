part of 'custom_widgets.dart';

class CustomShowSnackBar {
  static basicRedSnackBar(BuildContext context, String text) {
    print("value for snackbar $text");
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("$text"),
        behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.red,
        duration: Duration(seconds: 2)));
  }

  static basicGreenSnackBar(BuildContext context, String text) {
    print("value for snackbar $text ");
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("$text"),
        behavior: SnackBarBehavior.floating,
        backgroundColor: Colors.green,
        action: SnackBarAction(
          label: 'OK',
          textColor: Colors.white,
          onPressed: () {
            print('OK');
          },
        ),
        duration: Duration(seconds: 2)));
  }
}
