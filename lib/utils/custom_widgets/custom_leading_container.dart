part of 'custom_widgets.dart';

class CustomLeadingContainer extends StatelessWidget {
  final Color borderColor;
  final Widget iconCenter;

  const CustomLeadingContainer(
      {Key? key, required this.borderColor, required this.iconCenter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40,
      height: 40,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: borderColor, width: 3),
      ),
      child: Center(child: iconCenter),
    );
  }
}
