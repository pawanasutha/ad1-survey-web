part of '../custom_widgets.dart';

showWarningDialog(String content, context) {
  return showDialog(
      context: context,
      builder: (myContext) => AlertDialog(
            title: new Text('Warning'),
            content: new Text("$content"),
            actions: [
              new TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: new Text(
                    'OK',
                    style: TextStyle(color: Colors.lightBlue),
                  ))
            ],
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.white70, width: 1),
              borderRadius: BorderRadius.circular(10),
            ),
          ));
}
