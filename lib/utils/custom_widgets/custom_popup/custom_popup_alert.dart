part of '../custom_widgets.dart';

class CustomPopupAlert extends StatelessWidget {
  final String contentTittle;
  final String contentSubTittle;
  final String contentButton1;
  final String contentButton2;
  final Function() functionsButton1;
  final Function() functionsButton2;

  const CustomPopupAlert(
      {Key? key,
      required this.contentTittle,
      required this.contentSubTittle,
      required this.contentButton1,
      required this.contentButton2,
      required this.functionsButton1,
      required this.functionsButton2})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        contentTittle,
        style: poppinsw600sz24,
      ),
      content: SingleChildScrollView(
        child: ListBody(
          children: <Widget>[
            Text(
              contentSubTittle,
              style: nunitoSansw400sz16,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
      actions: <Widget>[
        TextButton(
          child: Text(contentButton1, style: nunitoSansw600sz16C0A66C2),
          onPressed: functionsButton1,
        ),
        Container(
          height: MediaQuery.of(context).size.height / 20,
          padding: EdgeInsets.symmetric(horizontal: 16),
          decoration: BoxDecoration(
              color: Colors.yellow, borderRadius: BorderRadius.circular(20)),
          child: TextButton(
            child: Text(contentButton2, style: nunitoSansw400sz14),
            onPressed: functionsButton2,
          ),
        ),
      ],
    );
  }
}
