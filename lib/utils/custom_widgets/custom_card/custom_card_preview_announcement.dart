part of '../custom_widgets.dart';

class CustomCardPreviewAnnouncement extends StatelessWidget {
  final ModelAnnouncement modelAnnouncement;

  const CustomCardPreviewAnnouncement(
      {Key? key, required this.modelAnnouncement})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return GestureDetector(
      onTap: () {
        debugPrint('Clicked on Notif ' + modelAnnouncement.title!);
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
              bottom: BorderSide(width: 1.0, color: Colors.grey),
            )),
        width: MediaQuery.of(context).size.width,
        padding:
            EdgeInsets.symmetric(vertical: 12, horizontal: _sizeScreen.wp(1)),
        //margin: EdgeInsets.symmetric(horizontal: _sizeScreen.sizeWidth(20)),
        child: Row(
          children: [
            Container(
              width: 30,
              height: 30,
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: Color(0xFF83B1E1)),
              child: Center(
                child: Image.asset(
                  'assets/icons/icon_document.png',
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(
              width: _sizeScreen.hp(1),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: _sizeScreen.wp(15),
                        child: Text(
                          modelAnnouncement.title!,
                          style: GoogleFonts.nunitoSans(
                              color: (modelAnnouncement.isRead)
                                  ? Colors.grey
                                  : Colors.black,
                              fontSize: 12,
                              fontWeight: FontWeight.bold),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      CustomChip(contentText: modelAnnouncement.status!)
                    ],
                  ),
                ),
                SizedBox(
                  height: _sizeScreen.hp(0.5),
                ),
                Container(
                    child: Row(
                  children: [
                    Container(
                      width: _sizeScreen.wp(15),
                      child: Text(
                        modelAnnouncement.message!,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: GoogleFonts.nunitoSans(
                            color: (modelAnnouncement.isRead)
                                ? Colors.grey
                                : Colors.black,
                            fontSize: 16),
                      ),
                    ),
                    Container(
                      height: _sizeScreen.hp(3.5),
                      width: _sizeScreen.wp(8),
                      child: Center(
                          child: Text(
                        modelAnnouncement.timeReceive!,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: GoogleFonts.nunitoSans(
                            color: (modelAnnouncement.isRead)
                                ? Colors.grey
                                : Colors.black,
                            fontSize: 16),
                      )),
                    ),
                  ],
                )),
              ],
            )
          ],
        ),
      ),
    );
  }
}
