part of '../custom_widgets.dart';

class CustomCardPreviewChat extends StatelessWidget {
  final ModelChat modelChat;

  const CustomCardPreviewChat({Key? key, required this.modelChat})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print('Clicked on Notif ' + modelChat.title!);
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
              bottom: BorderSide(width: 1.0, color: Colors.grey),
            )),
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
        //margin: EdgeInsets.symmetric(horizontal: _sizeScreen.sizeWidth(20)),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          modelChat.title!,
                          style: GoogleFonts.nunitoSans().copyWith(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color:
                                (modelChat.isRead) ? Colors.grey : Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                      child: Text(
                    modelChat.message!,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: GoogleFonts.nunitoSans().copyWith(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: (modelChat.isRead) ? Colors.grey : Colors.black,
                    ),
                  )),
                ],
              ),
            ),
            Container(
              child: Text(
                modelChat.timeReceive!,
                style: GoogleFonts.nunitoSans().copyWith(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: (modelChat.isRead) ? Colors.grey : Colors.black,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
