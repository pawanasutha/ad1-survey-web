part of '../custom_widgets.dart';

class CustomCardVerification extends StatelessWidget {
  final String imageAssetPath;
  final String textVerification;
  final String phoneNumber;
  final Function() function;

  const CustomCardVerification(
      {Key? key,
      required this.imageAssetPath,
      required this.textVerification,
      required this.phoneNumber,
      required this.function})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return InkWell(
      onTap: function,
      child: Container(
        padding: EdgeInsets.only(left: _sizeScreen.wp(1)),
        width: _sizeScreen.wp(24),
        height: _sizeScreen.hp(6),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            border: Border.all(color: Color(0xffC7C8CD))),
        child: Row(
          children: [
            Container(
              width: 24,
              height: 24,
              decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage(imageAssetPath))),
            ),
            SizedBox(
              width: _sizeScreen.wp(1.5),
            ),
            Container(
              width: _sizeScreen.wp(14),
              height: _sizeScreen.hp(2.5),
              child: Center(
                child: Text(
                  '$textVerification $phoneNumber',
                  style: nunitoSansw400sz14,
                  textAlign: TextAlign.left,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
            SizedBox(
              width: _sizeScreen.wp(2),
            ),
            Icon(Icons.navigate_next)
          ],
        ),
      ),
    );
  }
}
