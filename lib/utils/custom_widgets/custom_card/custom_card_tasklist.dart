part of '../custom_widgets.dart';

class CustomCardTaskList extends StatelessWidget {
  final ModelTaskList modelTaskList;

  const CustomCardTaskList({Key? key, required this.modelTaskList})
      : super(key: key);

  String getIconData(String objectType) {
    switch (objectType) {
      case "Motor Baru":
        return 'assets/icons/icons_product/icon_motor_baru.png';
      case "Motor Bekas":
        return 'assets/icons/icons_product/icon_motor_bekas.png';
      case "Mobil Baru":
        return 'assets/icons/icons_product/icon_mobil_baru.png';
      case "Mobil Bekas":
        return 'assets/icons/icons_product/icon_mobil_bekas.png';
      case "Multiguna":
        return 'assets/icons/icons_product/icon_dana_tunai.png';
      case "Durable":
        return 'assets/icons/icons_product/icon_durable.png';
    }
    return 'assets/icons/icons_product/icon_cross_file.png';
  }

  @override
  Widget build(BuildContext context) {
    var _sizeScreen = Screen(MediaQuery.of(context).size);
    return Consumer<HomeNotifier>(builder: (context, home, _) {
      return InkWell(
        onTap: () {
          debugPrint('Clicked on TaskList ${modelTaskList.orderNo}');
          home.isTaskListDataPreview = true;
        },
        child: Container(
          height: _sizeScreen.hp(7),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6.0),
            border: Border.all(color: Color(0xFEF6F6F6)),
            color: Colors.white,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                height: _sizeScreen.hp(4),
                width: _sizeScreen.wp(3),
                decoration: BoxDecoration(
                  color: Color(0xfeF6F6F6),
                  shape: BoxShape.circle,
                ),
                child: Center(
                    child: Image.asset(getIconData(modelTaskList.objectType!))),
              ),
              SizedBox(
                width: _sizeScreen.wp(0.25),
              ),
              Container(
                width: _sizeScreen.wp(6),
                child: Text(
                  modelTaskList.custName!,
                  style: nunitoSansw400sz14,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              SizedBox(
                width: _sizeScreen.wp(1),
              ),
              Container(
                width: _sizeScreen.wp(6),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(modelTaskList.objectType!,
                        style: nunitoSansw700sz12,
                        overflow: TextOverflow.ellipsis),
                    Text(
                        '${modelTaskList.objectBrand} ${modelTaskList.objectModel}',
                        style: nunitoSansw700sz12,
                        overflow: TextOverflow.ellipsis),
                  ],
                ),
              ),
              SizedBox(
                width: _sizeScreen.wp(1),
              ),
              Container(
                width: _sizeScreen.wp(7),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Senin , 24 Mei 2021',
                        style: nunitoSansw700sz12,
                        overflow: TextOverflow.ellipsis),
                    Text('Pkl 12.30',
                        style: nunitoSansw700sz12,
                        overflow: TextOverflow.ellipsis),
                  ],
                ),
              ),
              SizedBox(
                width: _sizeScreen.wp(1),
              ),
              Container(
                width: _sizeScreen.wp(7),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Selasa , 25 Mei 2021',
                        style: nunitoSansw700sz12,
                        overflow: TextOverflow.ellipsis),
                    Text('Pkl 10.30',
                        style: nunitoSansw700sz12,
                        overflow: TextOverflow.ellipsis),
                  ],
                ),
              ),
              SizedBox(
                width: _sizeScreen.wp(2),
              ),
              CustomChip(contentText: modelTaskList.lastKnownHandledBy!)
            ],
          ),
        ),
      );
    });
  }
}
