part of 'utils.dart';

class AppLevelConstants {
  static const String titleOnTab = 'AdiraOne Survey';

  ///LoginArea
  static const String loginHeaderText =
      'Selamat datang di aplikasi AdiraOne Survey. Mohon masukkan akun Anda.';
  static const String loginForgotPassword = 'Lupa Sandi?';
  static const String loginRememberMe = 'Ingat Saya';
  static const String loginButtonText = 'Masuk';

  ///ForgotPassword Area
  static const String forgotPasswordHeaderText =
      'Instruksi akan dikirimkan via email. Mohon untuk memasukan email yang terdaftar.';
  static const String forgotPasswordButtonText = 'Kirim Intruksi';

  ///Menu Options
  static const String homeRoute = '/';
  static const String forgotPasswordRoute = '/forgotPassword';
  static const String verificationRoute = '/verification';
  static const String verificationOTPRoute = '/verificationOTP';
  static const String dashboardRoute = '/dashBoard';
  static const String dashboardHomeRoute = '/dashBoard/home';
  static const String dashboardOrderTrackingRoute = '/dashBoard/orderTracking';
  static const String dashboardInsuranceRoute = '/dashBoard/insurance';
  static const String dashboardProfileRoute = '/dashBoard/home';
  static const String dashboardSettingRoute = '/dashBoard/settings';
}
